﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace ClassInventor
{

    public class Stock : ITreeViewStructure
    {

        public String OnderdeelNaam { get; set; }
        public int VoorRaad { get; set; }

        public Double InkoopPrijs { get; set; }

        public Double Prijs { get; set; }

        public String Model { get; set; }

        public String Merk { get; set; }

        public Double Marge { get; set; }

        public Double VerzendKoste { get; set; }

        public String Leverancier { get; set; }

        public int Min { get; set; }

        public Double Persooneels { get; set; }

        public String Color { get; set; }

        public TreeView TvMerken { get; set; }


        public Stock()
        {
            this.OnderdeelNaam = String.Empty;
            this.VoorRaad = 0;
            this.InkoopPrijs = 0;
            this.Prijs = 0;
            this.Model = String.Empty;
            this.Merk = String.Empty;
            this.Marge = 0;
            this.VerzendKoste = 0;
            this.Min = 0;
            this.Persooneels = 0;
            this.Color = String.Empty;
            this.TvMerken = null;
        }

        public Stock(String onderdeelNaam, int voorRaad, Double inkoopPrijs, Double prijs, String model, String merk,
            Double marge, Double verzendKoste,
            String leverancier, int min, Double persooneels, String color, TreeView tvMerken)
        {
            this.OnderdeelNaam = onderdeelNaam;
            this.VoorRaad = voorRaad;
            this.InkoopPrijs = inkoopPrijs;
            this.Prijs = prijs;
            this.Model = model;
            this.Merk = merk;
            this.Marge = marge;
            this.VerzendKoste = verzendKoste;
            this.Leverancier = leverancier;
            this.Min = min;
            this.Persooneels = persooneels;
            this.Color = color;
            this.TvMerken = tvMerken;
        }

        public Stock(String onderdeelNaam, int voorRaad, Double inkoopPrijs, Double prijs, String model, String merk,
            Double marge)
        {
            this.OnderdeelNaam = onderdeelNaam;
            this.VoorRaad = voorRaad;
            this.InkoopPrijs = inkoopPrijs;
            this.Prijs = prijs;
            this.Model = model;
            this.Merk = merk;
            this.Marge = marge;

        }



        public Stock(TreeView tvMerken)
        {
            this.TvMerken = tvMerken;
        }

        private string ReadFromCSVFile()
        {
            Tekstbestand bestand = new Tekstbestand();
            bestand.FileName = @"P:\MediaMarkt\HerstelBalie\ClassInventor\ClassInventor\ClassInventor\INVENTOR.csv";
            bestand.Lees();
            return bestand.Text;
        }

        public List<Stock> GetList()
        {

            string[] stocklist = this.ReadFromCSVFile().Split('\n');
            List<Stock> list = new List<Stock>();
            foreach (string s in stocklist)
            {
                list.Add(this.ToObject(s));
            }
            return list;


        }

        public void CreatingCsvFiles(Stock stock)
        {
            string[] stocklist = this.ReadFromCSVFile().Split('\n');
            foreach (var itm in stocklist)
            {
                if (itm != stocklist.ToString())
                {
                    string filePath = @"P:\MediaMarkt\HerstelBalie\ClassInventor\ClassInventor\ClassInventor\INVENTOR.csv";
                    if (!File.Exists(filePath))
                    {
                        File.Create(filePath).Close();
                    }
               

                    string[][] output = new string[][]{
            new string[]{ stock.Model, stock.OnderdeelNaam, stock.Color, stock.Leverancier,
                stock.Min.ToString() , stock.VoorRaad.ToString(),stock.Prijs.ToString(),
                stock.VerzendKoste.ToString(), stock.Marge.ToString(), stock.Persooneels .ToString()} /*add the values that you want inside a csv file. Mostly this function can be used in a foreach loop.*/
            };
                    int length = output.GetLength(0);
                    StringBuilder sb = new StringBuilder();
                    for (int index = 0; index < length; index++)
                        sb.AppendLine(String.Join("|",output[index]));
                    File.AppendAllText(filePath, sb.ToString());

                }

                else
                {
                    MessageBox.Show("Test");
                }
            }



        }

        public Stock ToObject(string line)
        {


            Stock stock = new Stock();
            string[] values = line.Split('|');


            stock.Model = values[0];
            stock.OnderdeelNaam = values[1];
            stock.Color = values[2];
            stock.Leverancier = values[3];

            stock.Min = int.Parse(values[4]);
            stock.VoorRaad = int.Parse(values[5]);

            stock.Prijs = Double.Parse(values[6]);
            stock.InkoopPrijs = Double.Parse(values[7]);
            stock.VerzendKoste = Double.Parse(values[8]);
            stock.Marge = double.Parse(values[9]);

            stock.Persooneels = Double.Parse(values[10]);



            return stock;
        }

        public void DrawNodeTreevieuw(TreeView tTvMerken)
        {
            tTvMerken = this.TvMerken;
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 4");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 4S");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 5");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 5S");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 6");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPHONE 6+");

            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPAD AIR");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPAD 4");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPAD 3");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPAD 2");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPAD MINI");

            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPOD TOUCH 4");
            tTvMerken.Nodes[0].Nodes[0].Nodes.Add("IPOD TOUCH 5");


            tTvMerken.Nodes[0].Nodes[1].Nodes.Add("GALAXY S5 MINI");
            tTvMerken.Nodes[0].Nodes[1].Nodes.Add("GALAXY S5");
            tTvMerken.Nodes[0].Nodes[1].Nodes.Add("GALAXY S6");
            tTvMerken.Nodes[0].Nodes[1].Nodes.Add("GALAXY S6 EDGE");
            tTvMerken.Nodes[0].Nodes[1].Nodes.Add("GALAXY S6 EDGE+");
        }

    }
}

