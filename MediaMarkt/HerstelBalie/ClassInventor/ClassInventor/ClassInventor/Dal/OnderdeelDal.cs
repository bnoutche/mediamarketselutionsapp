﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using ClassInventor.Model;


namespace ClassInventor.Dal
{
    public class OnderdeelDal : IDal<Model.Onderdeel>
    {
        protected List<HardWare> HardWareList;
        protected string message;

        private readonly string _conStr = ConfigurationManager.ConnectionStrings["InventorEntities"].ConnectionString;
        public string Message
        {
            get { return this.message; }
        }

        protected Model.Onderdeel model;
        public Model.Onderdeel Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        public OnderdeelDal(Model.Onderdeel model)
        {
            this.model = model;
        }

        public List<HardWare> GethardWareList()
        {
            HardWareList = new List<HardWare>();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["InventorEntities"].ToString();
            SqlCommand command = new SqlCommand();

            string sqlString = "HardwareList";

            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";

                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " HardWare rijen gevonden.";
                            while (result.Read())
                            {
                                ClassInventor.Model.HardWare hardWare =
                                    new Model.HardWare();
                                hardWare.Merk =
                                    (result.IsDBNull(result.GetOrdinal("Merk")) ? "" :
                                    result[result.GetOrdinal("Merk")].ToString());

                                hardWare.Model =
                                   (result.IsDBNull(result.GetOrdinal("Model")) ? "" :
                                   result[result.GetOrdinal("Model")].ToString());

                                hardWare.Type =
                                  (result.IsDBNull(result.GetOrdinal("Type")) ? "" :
                                  result[result.GetOrdinal("Type")].ToString());

                                hardWare.IdHardware =
                                    (result.IsDBNull(result.GetOrdinal("IdHardWare")) ? 0 :
                                    Int32.Parse(result[result.GetOrdinal("IdHardWare")].ToString()));

                                HardWareList.Add(hardWare);
                            }
                        }
                        else
                        {
                            this.message = " HardWare rijen niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return HardWareList;
        }



        public int Create()
        {


            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("OnderdeelInsert", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@Naam", Model.Naam));
                comdObj.Parameters.Add(new SqlParameter("@Option", Model.Option));
                comdObj.Parameters.Add(new SqlParameter("@Leverancier", Model.Leverancier));
                comdObj.Parameters.Add(new SqlParameter("@MinVoorRaad", Model.MinVoorRaad));
                comdObj.Parameters.Add(new SqlParameter("@VoorRaad", Model.VoorRaad));
                comdObj.Parameters.Add(new SqlParameter("@PrijsKlant", Model.PrijsKant));
                comdObj.Parameters.Add(new SqlParameter("@IkPrijs", Model.IkPrijs));
                comdObj.Parameters.Add(new SqlParameter("@Trans", Model.Trans));
                comdObj.Parameters.Add(new SqlParameter("@Marge", Model.Marge));
                comdObj.Parameters.Add(new SqlParameter("@PrijsPersoneel", Model.PrijsPersoneel));
                comdObj.Parameters.Add(new SqlParameter("@HardWareId", Model.HardWareId));
                comdObj.Parameters.Add(new SqlParameter("@IdOnderdeel", Model.IdOnderdeel));

                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();
                    if ((int)Model.IdOnderdeel == -100)
                    {
                        this.message = "Onderdeel bestaat al.";

                        result = -100;
                    }
                    else if (result <= 0)
                    {

                        this.message = " Onderdeel  is niet geïnserted.";
                    }
                    else
                    {
                        this.message = " Onderdeel is geïnserted.";
                        result = (int)Model.IdOnderdeel;
                    }
                }

                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result; // 0 of de Id van de nieuw

            }
        }

        public int Delete()
        {

            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("OndereelDelete", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@IdOnderdeel", Model.IdOnderdeel));

                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();

                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result;

            }

        }

        public List<Model.Onderdeel> ReadAll()
        {

            List<Model.Onderdeel> list = new List<Model.Onderdeel>();

            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["InventorEntities"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen de naam
            // van de stored procedure
            string sqlString = "OnderdeelList";

            // zeg aan het command object dat het een tored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result;
            using (connection)
            {
                try
                {
                    connection.Open();
                    {
                        // retourneert het aantal rijen dat geïnserted werd
                        this.message = "De database is klaar!";
                        // voeg using toe om er voor te zorgen dat
                        // de datareader gesloten wordt als we die niet
                        // meer nodig hebben
                        using (result = command.ExecuteReader())
                        {
                            if (result.HasRows)
                            {
                                // zolang dat er iets te lezen valt
                                // uit de tabel
                                while (result.Read())
                                {
                                    Model.Onderdeel onderdeel = new Model.Onderdeel();
                                    onderdeel.IdOnderdeel = (int)result["IdOnderdeel"];
                                    onderdeel.Naam = result["Naam"].ToString();
                                    onderdeel.Option = result["Option"].ToString();
                                    onderdeel.Leverancier = result["Leverancier"].ToString();
                                    onderdeel.MinVoorRaad = (int)result["MinVoorRaad"];
                                    onderdeel.VoorRaad = (int)result["VoorRaad"];

                                    // IIF immediate if
                                    // condition expression ? true : false
                                    onderdeel.PrijsKant = (Convert.IsDBNull(result["PrijsKlant"]) ?
                                        0f : float.Parse(result["PrijsKlant"].ToString()));
                                    onderdeel.IkPrijs = (Convert.IsDBNull(result["IkPrijs"]) ?
                                    0f : float.Parse(result["IkPrijs"].ToString()));

                                    onderdeel.Trans = (Convert.IsDBNull(result["Trans"]) ?
                                  0f : float.Parse(result["Trans"].ToString()));

                                    onderdeel.Marge = (Convert.IsDBNull(result["Marge"]) ?
                              0f : float.Parse(result["Marge"].ToString()));

                                    onderdeel.PrijsPersoneel = (Convert.IsDBNull(result["PrijsPersoneel"]) ?
                          0f : float.Parse(result["PrijsPersoneel"].ToString()));

                                    onderdeel.HardWareId = (int)result["HardWareId"];

                                    onderdeel.Trans = (float) Math.Round(onderdeel.Trans, 2, MidpointRounding.AwayFromZero);
                                    onderdeel.Marge = (float)Math.Round(onderdeel.Marge, 2, MidpointRounding.AwayFromZero);
                                    onderdeel.PrijsPersoneel = (float)Math.Round(onderdeel.PrijsPersoneel, 2, MidpointRounding.AwayFromZero);
                                    list.Add(onderdeel);
                                }
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return list;





           

                       
        }

        public Model.Onderdeel ReadOne()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["InventorEntities"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen we de naam
            // van de stored procedure
            string sqlString = "OnderdeelSelectOne";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Model.IdOnderdeel;
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";
                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " Onderdeel is gevonden.";
                            result.Read();
                            Model.Naam =
                                     (result.IsDBNull(result.GetOrdinal("Naam")) ? "" :
                                     result[result.GetOrdinal("Naam")].ToString());

                            Model.Option =
                               (result.IsDBNull(result.GetOrdinal("Option")) ? "" :
                               result[result.GetOrdinal("Option")].ToString());

                            Model.Leverancier =
                              (result.IsDBNull(result.GetOrdinal("Leverancier")) ? "" :
                              result[result.GetOrdinal("Leverancier")].ToString());


                            Model.MinVoorRaad =
                            (result.IsDBNull(result.GetOrdinal("MinVoorRaad")) ? 0 :
                            Int32.Parse(result[result.GetOrdinal("MinVoorRaad")].ToString()));


                            Model.VoorRaad =
                            (result.IsDBNull(result.GetOrdinal("VoorRaad")) ? 0 :
                            Int32.Parse(result[result.GetOrdinal("VoorRaad")].ToString()));

                            Model.PrijsKant =
                            (result.IsDBNull(result.GetOrdinal("PrijsKant")) ? 0 :
                            float.Parse(result[result.GetOrdinal("PrijsKant")].ToString()));

                            Model.IkPrijs =
                             (result.IsDBNull(result.GetOrdinal("IkPrijs")) ? 0 :
                             float.Parse(result[result.GetOrdinal("IkPrijs")].ToString()));

                            Model.Trans =
                            (result.IsDBNull(result.GetOrdinal("Trans")) ? 0 :
                            float.Parse(result[result.GetOrdinal("Trans")].ToString()));

                            Model.Marge =
                            (result.IsDBNull(result.GetOrdinal("Marge")) ? 0 :
                            float.Parse(result[result.GetOrdinal("Marge")].ToString()));

                            Model.PrijsPersoneel =
                         (result.IsDBNull(result.GetOrdinal("PrijsPersoneel")) ? 0 :
                         float.Parse(result[result.GetOrdinal("PrijsPersoneel")].ToString()));

                            Model.HardWareId =
             (result.IsDBNull(result.GetOrdinal("HardWareId")) ? 0 :
             int.Parse(result[result.GetOrdinal("HardWareId")].ToString()));

                            Model.IdOnderdeel =
                                (result.IsDBNull(result.GetOrdinal("IdOnderdeel")) ? 0 :
                                Int32.Parse(result[result.GetOrdinal("IdOnderdeel")].ToString()));
                        }
                        else
                        {
                            this.message = " Onderdeel rij niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return Model;
        }

        public int Update()
        {
            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("OnderdeelUpdate", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@Naam", Model.Naam));
                comdObj.Parameters.Add(new SqlParameter("@Option", Model.Option));
                comdObj.Parameters.Add(new SqlParameter("@Leverancier", Model.Leverancier));
                comdObj.Parameters.Add(new SqlParameter("@MinVoorRaad", Model.MinVoorRaad));
                comdObj.Parameters.Add(new SqlParameter("@VoorRaad", Model.VoorRaad));
                comdObj.Parameters.Add(new SqlParameter("@PrijsKlant", Model.PrijsKant));
                comdObj.Parameters.Add(new SqlParameter("@IkPrijs", Model.IkPrijs));
                comdObj.Parameters.Add(new SqlParameter("@Trans", Model.Trans));
                comdObj.Parameters.Add(new SqlParameter("@Marge", Model.Marge));
                comdObj.Parameters.Add(new SqlParameter("@PrijsPersoneel", Model.PrijsPersoneel));
                comdObj.Parameters.Add(new SqlParameter("@HardWareId", Model.HardWareId));
                comdObj.Parameters.Add(new SqlParameter("@IdOnderdeel", Model.IdOnderdeel));
                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();

                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result;
            }



        }
    }
}
