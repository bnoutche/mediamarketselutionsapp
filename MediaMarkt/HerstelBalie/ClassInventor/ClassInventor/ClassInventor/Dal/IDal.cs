﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassInventor.Model;

namespace ClassInventor.Dal { 

        interface IDal<T>
        {
            string Message { get; }
            int Create();
            int Update();
            int Delete();
            T ReadOne();
            List<T> ReadAll();
        }
    }

