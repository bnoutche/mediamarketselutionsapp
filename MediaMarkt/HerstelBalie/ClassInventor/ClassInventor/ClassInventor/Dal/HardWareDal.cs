﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using ClassInventor.Model;

namespace ClassInventor.Dal
{
    public class HardWareDal : IDal<Model.HardWare>
    {
        private readonly string _conStr = ConfigurationManager.ConnectionStrings["InventorEntities"].ConnectionString;
        protected string message;

        public string Message
        {
            get { return this.message; }
        }

        public int Create()
        {
            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("HardWareInsert", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@Model", Model.Model));
                comdObj.Parameters.Add(new SqlParameter("@Merk", Model.Merk));
                comdObj.Parameters.Add(new SqlParameter("@Type", Model.Type));
                comdObj.Parameters.Add(new SqlParameter("@IdHardWare", Model.IdHardware));
                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();
                    if ((int) Model.IdHardware == -100)
                    {
                        this.message = "Device bestaat al.";

                        result = -100;
                    }
                    else if (result <= 0)
                    {

                        this.message = " Device  is niet geïnserted.";
                    }
                    else
                    {
                        this.message = " Device is geïnserted.";
                        result = (int) Model.IdHardware;
                    }
                }

                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result; // 0 of de Id van de nieuw
            }
        
    }

        public int Update()
        {

            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("HardWareUpdate", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@Model", Model.Model));
                comdObj.Parameters.Add(new SqlParameter("@Merk", Model.Merk));
                comdObj.Parameters.Add(new SqlParameter("@Type", Model.Type));
                comdObj.Parameters.Add(new SqlParameter("@IdHardWare", Model.IdHardware));
                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();

                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result;
            }
           
        }

        public int Delete()
        {

            int result = 0;
            using (SqlConnection concObj = new SqlConnection(_conStr))
            {
                SqlCommand comdObj = new SqlCommand("HardWareDelete", concObj)
                {
                    CommandType = CommandType.StoredProcedure
                };
                comdObj.Parameters.Add(new SqlParameter("@IdHardWare", Model.IdHardware));

                try
                {
                    concObj.Open();
                    result = comdObj.ExecuteNonQuery();

                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }

                return result;

            }
         
        }

        public HardWare ReadOne()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["InventorEntities"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen we de naam
            // van de stored procedure
            string sqlString = "HardWareSelectOne";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Model.IdHardware;
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";
                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " HardWare is gevonden.";
                            result.Read();
                            Model.Merk =
                                (result.IsDBNull(result.GetOrdinal("Merk"))
                                    ? ""
                                    : result.GetString(result.GetOrdinal("Merk")));
                            Model.Model =
                                (result.IsDBNull(result.GetOrdinal("Model"))
                                    ? ""
                                    : result.GetString(result.GetOrdinal("Model")));

                            Model.Type =
                                (result.IsDBNull(result.GetOrdinal("Type"))
                                    ? ""
                                    : result.GetString(result.GetOrdinal("Type")));

                            Model.IdHardware =
                                (result.IsDBNull(result.GetOrdinal("IdHardWare"))
                                    ? 0
                                    : result.GetInt32(result.GetOrdinal("IdHardWare")));
                        }
                        else
                        {
                            this.message = " HardWare rij niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return Model;
        }

        public List<Model.HardWare> ReadAll(){
  
            List<Model.HardWare> list = new List<Model.HardWare>();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["InventorEntities"].ToString();


            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen we de naam
            // van de stored procedure
            string sqlString = "HardwareList ";
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";

                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " HardWare rijen gevonden.";
                            while (result.Read())
                            {
                                ClassInventor.Model.HardWare hardWare =
                                    new Model.HardWare();
                                hardWare.Merk =
                                    (result.IsDBNull(result.GetOrdinal("Merk")) ? "" :
                                    result[result.GetOrdinal("Merk")].ToString());

                                hardWare.Model =
                                   (result.IsDBNull(result.GetOrdinal("Model")) ? "" :
                                   result[result.GetOrdinal("Model")].ToString());

                                hardWare.Type =
                                  (result.IsDBNull(result.GetOrdinal("Type")) ? "" :
                                  result[result.GetOrdinal("Type")].ToString());

                                hardWare.IdHardware =
                                    (result.IsDBNull(result.GetOrdinal("IdHardWare")) ? 0 :
                                    Int32.Parse(result[result.GetOrdinal("IdHardWare")].ToString()));

                                list.Add(hardWare);
                            }
                        }
                        else
                        {
                            this.message = " HardWare rijen niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return list;
        }

        protected Model.HardWare model;
        public Model.HardWare Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        public HardWareDal(Model.HardWare model)
        {
            this.model = model;
        }
    }
}
