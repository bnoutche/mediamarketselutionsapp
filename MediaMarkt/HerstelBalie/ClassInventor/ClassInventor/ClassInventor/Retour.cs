﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassInventor
{
    public class Retour : Stock
    {
        public int Aantal { get; set; }
        public Boolean Doa { get; set; }
        public Boolean Od { get; set; }
        public String Refrentie { get; set; }



        public String Reden()
        {
            string reden = "";
            if (this.Doa == true) reden = "DOA";
            if (this.Od == true) reden = "OD";

            return reden;

        }



    }
}
