﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ClassInventor.Model
{
    public class Onderdeel
    {
        protected int idOnderdeel;
        protected String naam;
        protected String option;
        protected String leverancier;
        protected int minVoorRaad;
        protected int voorRaad;
        protected float prijsKant;
        protected float ikPrijs;
        protected float trans;
        protected float marge;
        protected float prijsPersoneel;
        protected int hardWareId;


        public int HardWareId
        {
            get { return hardWareId; }
            set { hardWareId = value; }
        }
        public float PrijsPersoneel
        {
            get { return prijsPersoneel; }
            set { prijsPersoneel = value; }
        }
        public float Marge
        {
            get { return marge; }
            set { marge = value; }
        }
        public float Trans
        {
            get { return trans; }
            set
            { trans = value; }
        }
        public float IkPrijs
        {
            get { return ikPrijs; }
            set { ikPrijs = value; }
        }
        public float PrijsKant
        {
            get { return prijsKant; }
            set { prijsKant = value; }
        }
        public int VoorRaad
        {
            get { return voorRaad; }
            set { voorRaad = value; }
        }
        public int MinVoorRaad
        {
            get { return minVoorRaad; }
            set { minVoorRaad = value; }
        }
        public string Leverancier
        {
            get { return leverancier; }
            set { leverancier = value; }
        }
        public int IdOnderdeel
        {
            get { return idOnderdeel; }
            set { idOnderdeel = value; }
        }

        public string Naam
        {
            get { return naam; }
            set { naam = value; }
        }

        public string Option
        {
            get { return option; }
            set { option = value; }
        }

        public virtual ICollection<HardWare> HardWere { get; set; }

        public override string ToString()
        {


            return "Part: " + this.naam.ToUpper() +Environment.NewLine
                + " Option: " + this.option.ToUpper() + Environment.NewLine
                + " Supplier: " + this.Leverancier.ToUpper() + Environment.NewLine
                + " Min: " +this.minVoorRaad + " Stock: " + this.voorRaad + " Price: " + this.prijsKant .ToString("c") + " Ik: " + this.ikPrijs.ToString("c") +
                   " Trans: " + this.trans.ToString("c") + " Marge: " + this.marge +"%" + " StaffPrice: " + this.PrijsPersoneel.ToString("c");
        }
    }
}
