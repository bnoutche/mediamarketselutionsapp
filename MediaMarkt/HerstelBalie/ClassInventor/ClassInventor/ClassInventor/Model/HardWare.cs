﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassInventor.Model
{
   public class HardWare
   {
       protected String merk;
       protected String model;
       protected String type;

       public string Type
       {
           get { return type; }
           set { type = value; }
       }

        public Int32 IdHardware { get; set; }

       public string Merk
       {
           get { return merk; }
           set { merk = value; }
       }

       public string Model
       {
           get { return model; }
           set { model = value; }
       }

   }
}
