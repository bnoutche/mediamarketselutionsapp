﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassInventor.Model
{
   public class RegisterDevice : Model.Onderdeel
    {
       public int Aantal { get; set; }
       public int RegisteredDevice()
       {
           return this.VoorRaad - this.Aantal;
       }
    }
}
