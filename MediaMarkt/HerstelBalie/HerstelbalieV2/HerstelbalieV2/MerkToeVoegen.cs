﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassInventor;

namespace HerstelbalieV2
{
    public partial class MerkToeVoegen : Form
    {
        public Stock Stock
        {
            get
            {
                var tmp = new Stock() {Merk = txt_MerkNaam.Text};

                return tmp;
            }
        }
        public MerkToeVoegen()
        {
            InitializeComponent();
        }
    }
}
