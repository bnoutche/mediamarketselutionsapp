﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassInventor;

namespace HerstelbalieV2
{
    public partial class StokAanVullen : Form
    {
        private List<Leverancier> _leveranciers;

        private Stock _stock;
        public Stock StockAanvullen
        {
            get
            {
                Stock tmp = (Stock) _stock;

                tmp.Model = lbl_model.Text;
                tmp.OnderdeelNaam = txt_OnderdeelNaam.Text;
                tmp.Color = Txt_option.Text;
                tmp.Leverancier = cbo_Lerverancier.SelectedText;
                tmp.Min = int.Parse(txt_min.Text);
                tmp.VoorRaad = int.Parse(txt_Stock.Text);
                tmp.Prijs = Double.Parse(txt_Prijs.Text);
                tmp.InkoopPrijs = Double.Parse(txt_InkoopPrijs.Text);
                tmp.VerzendKoste = Double.Parse(txt_VerzendKoste.Text);
                tmp.Marge = Double.Parse(txt_Marge.Text);
                tmp.Persooneels = Double.Parse(txt_personeel.Text);
                return tmp;
            }
        }
        public StokAanVullen()
        {
            InitializeComponent();
           _leveranciers = Enum.GetValues(typeof(Leverancier))
                                                .Cast<Leverancier>()
                                                .ToList();
            
            VulLeveranciers();

        }

        public StokAanVullen(Stock stock):this()
        {
            _stock = stock;

            lbl_model.Text = _stock.Model;
            txt_OnderdeelNaam.Text = _stock.OnderdeelNaam;
            Txt_option.Text = _stock.Color;
            cbo_Lerverancier.SelectedIndex = cbo_Lerverancier.FindString(_stock.Leverancier);
            txt_min.Text = _stock.Min.ToString();
            txt_Stock.Text = _stock.VoorRaad.ToString();
            txt_Prijs.Text = _stock.Prijs.ToString();
            txt_InkoopPrijs.Text = _stock.InkoopPrijs.ToString();
            txt_VerzendKoste.Text = _stock.VerzendKoste.ToString();
            txt_Marge.Text = _stock.Marge.ToString();
            txt_personeel.Text = _stock.Persooneels.ToString();

        }


        public void VulLeveranciers()
        {
            foreach (var itm in _leveranciers )
            {
                cbo_Lerverancier.Items.Add(itm);
            }
        }
    }
}
