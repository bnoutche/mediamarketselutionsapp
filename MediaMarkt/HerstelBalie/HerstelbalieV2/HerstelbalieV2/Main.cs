﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassInventor;

namespace HerstelbalieV2
{


    public partial class InventorApp : Form
    {

        private List<Stock> _inhoudList;

        private List<Toestel> _toestellen;

        private List<Merken> _merken;

        ListViewItem _tmpItem = null;



        public InventorApp()
        {
            InitializeComponent();

            _inhoudList = new List<Stock>();
            LaadBestand();
            LijstenVullen();
            VulMerken();


            VulToestel();
            cob_Toestel.SelectedIndex = 0;

            // TopTitle();
            Modeltoevoegen();
        }

        public void LijstenVullen()
        {
            _toestellen = Enum.GetValues(typeof(Toestel))
                .Cast<Toestel>()
                .ToList();

            _merken = Enum.GetValues(typeof(Merken))
                .Cast<Merken>()
                .ToList();

        }

        public void VulToestel()
        {
            foreach (var itm in _toestellen)
            {
                cob_Toestel.Items.Add(itm);
            }

        }

        public void VulMerken()
        {

            TreeNode merkenNode = new TreeNode("Merken");
            tv_Merken.Nodes.Add(merkenNode);


            foreach (var itm in _merken)
            {

                tv_Merken.Nodes[0].Nodes.Add(itm.ToString());

            }

            tv_Merken.ExpandAll();

        }

        public void Modeltoevoegen()
        {
            try
            {

                Stock stock = new Stock(tv_Merken);
                stock.DrawNodeTreevieuw(tv_Merken);

            }

            catch (Exception)
            {


            }
        }


        private void btn_stockAanvullen_Click(object sender, EventArgs e)
        {
            Stock stock = new Stock();

            if (lv_Inhoud.SelectedItems.Count > 0)
            {
                string naam = tv_Merken.SelectedNode.Text;

                
                ListViewItem onderdeelNaam = lv_Inhoud.SelectedItems[0];

                var option = onderdeelNaam.SubItems[1].Text;
                var leverancier = onderdeelNaam.SubItems[2].Text;

                var min = onderdeelNaam.SubItems[3].Text;
                var voorRaad = onderdeelNaam.SubItems[4].Text;

                var prijs = onderdeelNaam.SubItems[5].Text;
                var inkoopPrijs = onderdeelNaam.SubItems[6].Text;

                var verzendkoste = onderdeelNaam.SubItems[7].Text;
                var marge = onderdeelNaam.SubItems[8].Text;
                var personeel = onderdeelNaam.SubItems[9].Text;
                
                stock.Model = naam;
                stock.OnderdeelNaam = onderdeelNaam.Text;
                stock.Color = option;
                stock.Leverancier = leverancier;
                stock.Min = int.Parse(min);
                stock.VoorRaad = int.Parse(voorRaad);
                stock.Prijs = Double.Parse(prijs);
                stock.InkoopPrijs = Double.Parse(inkoopPrijs);
                stock.VerzendKoste = Double.Parse(verzendkoste);
                stock.Marge = Double.Parse(marge);
                stock.Persooneels = Double.Parse(personeel);


            }

            StokAanVullen aanVullen = new StokAanVullen(stock);
            aanVullen.ShowDialog();

            if (aanVullen.DialogResult == DialogResult.OK)
            {

                stock.CreatingCsvFiles(stock);
            }
        }

        private
            void btn_nieuwMerk_Click(object sender, EventArgs e)
        {
            MerkToeVoegen mt = new MerkToeVoegen();
            mt.ShowDialog();

            if (mt.DialogResult == DialogResult.OK)
            {
                TreeNode newMerk = new TreeNode(mt.Stock.Merk);
                tv_Merken.Nodes[0].Nodes.Add(newMerk);


            }
        }


        private void btn_nieuwModel_Click(object sender, EventArgs e)
        {

            ModelToevoegen mt = new ModelToevoegen();
            mt.ShowDialog();

            if (mt.DialogResult == DialogResult.OK)
            {

                foreach (TreeNode node in tv_Merken.Nodes[0].Nodes)
                {
                    if (node.ToString().ToUpper().Contains(mt.Stock.Merk.ToUpper()))
                    {
                        node.Nodes.Add(mt.Stock.Model);

                    }


                }


            }




        }

        private void tv_Merken_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
            
            lv_Inhoud.Items.Clear();
            lv_Inhoud.Columns.Clear();
       
            
            if (_inhoudList != null)
            {
                lv_Inhoud.View = View.Details;
                lv_Inhoud.Columns.Add("Onderdeel", 150, HorizontalAlignment.Left);
                lv_Inhoud.Columns.Add("Option", 150, HorizontalAlignment.Left);
                lv_Inhoud.Columns.Add("Lerverancier", 150, HorizontalAlignment.Left);
                lv_Inhoud.Columns.Add("Min", 100, HorizontalAlignment.Left);
                lv_Inhoud.Columns.Add("Stock", 100, HorizontalAlignment.Center);
                lv_Inhoud.Columns.Add("Prijs", 100, HorizontalAlignment.Center);
                lv_Inhoud.Columns.Add("Ik", 100, HorizontalAlignment.Center);
                lv_Inhoud.Columns.Add("Trans", 100, HorizontalAlignment.Center);
                lv_Inhoud.Columns.Add("Marge", 100, HorizontalAlignment.Center);
                lv_Inhoud.Columns.Add("Personeels", 150, HorizontalAlignment.Center);
                foreach (Stock stockItm in _inhoudList)
                {

                 
                    
                    string naam = tv_Merken.SelectedNode.Text;
                    if (naam.ToUpper() == stockItm.Model.ToUpper())
                    {



                        //
                        _tmpItem = lv_Inhoud.Items.Add(stockItm.OnderdeelNaam);
                        _tmpItem.SubItems.Add(stockItm.Color);
                        _tmpItem.SubItems.Add(stockItm.Leverancier);
                        _tmpItem.SubItems.Add(stockItm.Min.ToString());
                        _tmpItem.SubItems.Add(stockItm.VoorRaad.ToString());
                        _tmpItem.SubItems.Add(stockItm.Prijs.ToString());
                        _tmpItem.SubItems.Add(stockItm.InkoopPrijs.ToString());
                        _tmpItem.SubItems.Add(stockItm.VerzendKoste.ToString());
                        _tmpItem.SubItems.Add(stockItm.Marge.ToString());
                        _tmpItem.SubItems.Add(stockItm.Persooneels.ToString());

                    }



                }


            }
        }


        public void LaadBestand()
        {
            Stock newStock = new Stock();

            _inhoudList = new List<Stock>();


                foreach (var itm in newStock.GetList())
                {
                    _inhoudList.Add(itm);
                }


        }
        private void btn_Export_Click(object sender, EventArgs e) { 

         
            
        }

        private void InventorApp_Load(object sender, EventArgs e)
        {
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        }
    }
}



