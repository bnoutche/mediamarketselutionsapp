﻿namespace HerstelbalieV2
{
    partial class ModelToevoegen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Opslaan = new System.Windows.Forms.Button();
            this.txt_ModelNaam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_MerkNaam = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_Opslaan
            // 
            this.btn_Opslaan.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Opslaan.Location = new System.Drawing.Point(298, 101);
            this.btn_Opslaan.Name = "btn_Opslaan";
            this.btn_Opslaan.Size = new System.Drawing.Size(75, 23);
            this.btn_Opslaan.TabIndex = 5;
            this.btn_Opslaan.Text = "Opslaan";
            this.btn_Opslaan.UseVisualStyleBackColor = true;
            // 
            // txt_ModelNaam
            // 
            this.txt_ModelNaam.Location = new System.Drawing.Point(76, 63);
            this.txt_ModelNaam.Name = "txt_ModelNaam";
            this.txt_ModelNaam.Size = new System.Drawing.Size(297, 20);
            this.txt_ModelNaam.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Merk";
            // 
            // txt_MerkNaam
            // 
            this.txt_MerkNaam.Location = new System.Drawing.Point(76, 37);
            this.txt_MerkNaam.Name = "txt_MerkNaam";
            this.txt_MerkNaam.Size = new System.Drawing.Size(297, 20);
            this.txt_MerkNaam.TabIndex = 7;
            // 
            // ModelToevoegen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 136);
            this.Controls.Add(this.txt_MerkNaam);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_Opslaan);
            this.Controls.Add(this.txt_ModelNaam);
            this.Controls.Add(this.label1);
            this.Name = "ModelToevoegen";
            this.Text = "ModelToevoegen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Opslaan;
        private System.Windows.Forms.TextBox txt_ModelNaam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_MerkNaam;
    }
}