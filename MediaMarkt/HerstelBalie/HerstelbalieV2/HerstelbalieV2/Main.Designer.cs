﻿namespace HerstelbalieV2
{
    partial class InventorApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_inschrijven = new System.Windows.Forms.TabPage();
            this.btn_Export = new System.Windows.Forms.Button();
            this.lsb_lijstHerstelingen = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gb_gegevens = new System.Windows.Forms.GroupBox();
            this.btn_verwijderen = new System.Windows.Forms.Button();
            this.btn_Opslaan = new System.Windows.Forms.Button();
            this.txt_marge = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_InKoopPrijs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_OnderdeelNaam = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Model = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Merk = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cob_Toestel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_Refentie = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_stockAanvullen = new System.Windows.Forms.Button();
            this.btn_nieuwModel = new System.Windows.Forms.Button();
            this.btn_nieuwMerk = new System.Windows.Forms.Button();
            this.lv_Inhoud = new System.Windows.Forms.ListView();
            this.tv_Merken = new System.Windows.Forms.TreeView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lsb_OverzichtDodOfOd = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groep = new System.Windows.Forms.GroupBox();
            this.rdb_OwnDamage = new System.Windows.Forms.RadioButton();
            this.rdb_reDOA = new System.Windows.Forms.RadioButton();
            this.btn_reVerwijderen = new System.Windows.Forms.Button();
            this.btn_reOpslaan = new System.Windows.Forms.Button();
            this.txt_reInkoopPrijs = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_reOnderdeelNaam = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_re_Model = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_reMerk = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cob_retoestel = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txt_reRefrentie = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tp_inschrijven.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gb_gegevens.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groep.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tp_inschrijven);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(14, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(850, 552);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 0;
            // 
            // tp_inschrijven
            // 
            this.tp_inschrijven.Controls.Add(this.btn_Export);
            this.tp_inschrijven.Controls.Add(this.lsb_lijstHerstelingen);
            this.tp_inschrijven.Controls.Add(this.groupBox1);
            this.tp_inschrijven.Location = new System.Drawing.Point(4, 22);
            this.tp_inschrijven.Name = "tp_inschrijven";
            this.tp_inschrijven.Padding = new System.Windows.Forms.Padding(3);
            this.tp_inschrijven.Size = new System.Drawing.Size(842, 526);
            this.tp_inschrijven.TabIndex = 0;
            this.tp_inschrijven.Text = "Inschrijven";
            this.tp_inschrijven.UseVisualStyleBackColor = true;
            // 
            // btn_Export
            // 
            this.btn_Export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Export.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_Export.Location = new System.Drawing.Point(629, 478);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(200, 45);
            this.btn_Export.TabIndex = 2;
            this.btn_Export.Text = "Export";
            this.btn_Export.UseVisualStyleBackColor = true;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // lsb_lijstHerstelingen
            // 
            this.lsb_lijstHerstelingen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lsb_lijstHerstelingen.FormattingEnabled = true;
            this.lsb_lijstHerstelingen.Location = new System.Drawing.Point(438, 11);
            this.lsb_lijstHerstelingen.Name = "lsb_lijstHerstelingen";
            this.lsb_lijstHerstelingen.Size = new System.Drawing.Size(391, 459);
            this.lsb_lijstHerstelingen.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.gb_gegevens);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(426, 514);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nieuw Inschrijving";
            // 
            // gb_gegevens
            // 
            this.gb_gegevens.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.gb_gegevens.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gb_gegevens.Controls.Add(this.btn_verwijderen);
            this.gb_gegevens.Controls.Add(this.btn_Opslaan);
            this.gb_gegevens.Controls.Add(this.txt_marge);
            this.gb_gegevens.Controls.Add(this.label7);
            this.gb_gegevens.Controls.Add(this.txt_InKoopPrijs);
            this.gb_gegevens.Controls.Add(this.label6);
            this.gb_gegevens.Controls.Add(this.textBox1);
            this.gb_gegevens.Controls.Add(this.label5);
            this.gb_gegevens.Controls.Add(this.txt_OnderdeelNaam);
            this.gb_gegevens.Controls.Add(this.label4);
            this.gb_gegevens.Controls.Add(this.txt_Model);
            this.gb_gegevens.Controls.Add(this.label3);
            this.gb_gegevens.Controls.Add(this.txt_Merk);
            this.gb_gegevens.Controls.Add(this.label2);
            this.gb_gegevens.Controls.Add(this.cob_Toestel);
            this.gb_gegevens.Controls.Add(this.label1);
            this.gb_gegevens.Location = new System.Drawing.Point(6, 123);
            this.gb_gegevens.Name = "gb_gegevens";
            this.gb_gegevens.Size = new System.Drawing.Size(414, 385);
            this.gb_gegevens.TabIndex = 3;
            this.gb_gegevens.TabStop = false;
            this.gb_gegevens.Text = "Gegevens";
            // 
            // btn_verwijderen
            // 
            this.btn_verwijderen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_verwijderen.Location = new System.Drawing.Point(262, 324);
            this.btn_verwijderen.Name = "btn_verwijderen";
            this.btn_verwijderen.Size = new System.Drawing.Size(137, 48);
            this.btn_verwijderen.TabIndex = 12;
            this.btn_verwijderen.Text = "Verwijderd";
            this.btn_verwijderen.UseVisualStyleBackColor = true;
            // 
            // btn_Opslaan
            // 
            this.btn_Opslaan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Opslaan.Location = new System.Drawing.Point(105, 324);
            this.btn_Opslaan.Name = "btn_Opslaan";
            this.btn_Opslaan.Size = new System.Drawing.Size(137, 48);
            this.btn_Opslaan.TabIndex = 12;
            this.btn_Opslaan.Text = "Opslaan";
            this.btn_Opslaan.UseVisualStyleBackColor = true;
            // 
            // txt_marge
            // 
            this.txt_marge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_marge.Location = new System.Drawing.Point(105, 248);
            this.txt_marge.Name = "txt_marge";
            this.txt_marge.Size = new System.Drawing.Size(87, 20);
            this.txt_marge.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Marge";
            // 
            // txt_InKoopPrijs
            // 
            this.txt_InKoopPrijs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_InKoopPrijs.Location = new System.Drawing.Point(105, 222);
            this.txt_InKoopPrijs.Name = "txt_InKoopPrijs";
            this.txt_InKoopPrijs.Size = new System.Drawing.Size(87, 20);
            this.txt_InKoopPrijs.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "InkoopPrijs";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(105, 196);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(87, 20);
            this.textBox1.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Aantal";
            // 
            // txt_OnderdeelNaam
            // 
            this.txt_OnderdeelNaam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_OnderdeelNaam.Location = new System.Drawing.Point(105, 152);
            this.txt_OnderdeelNaam.Name = "txt_OnderdeelNaam";
            this.txt_OnderdeelNaam.Size = new System.Drawing.Size(294, 20);
            this.txt_OnderdeelNaam.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "OnderdeelNaam";
            // 
            // txt_Model
            // 
            this.txt_Model.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Model.Location = new System.Drawing.Point(105, 115);
            this.txt_Model.Name = "txt_Model";
            this.txt_Model.Size = new System.Drawing.Size(294, 20);
            this.txt_Model.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Model";
            // 
            // txt_Merk
            // 
            this.txt_Merk.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Merk.Location = new System.Drawing.Point(105, 77);
            this.txt_Merk.Name = "txt_Merk";
            this.txt_Merk.Size = new System.Drawing.Size(294, 20);
            this.txt_Merk.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Merk";
            // 
            // cob_Toestel
            // 
            this.cob_Toestel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cob_Toestel.FormattingEnabled = true;
            this.cob_Toestel.Location = new System.Drawing.Point(105, 41);
            this.cob_Toestel.Name = "cob_Toestel";
            this.cob_Toestel.Size = new System.Drawing.Size(294, 21);
            this.cob_Toestel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Toestel";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.txt_Refentie);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(396, 77);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Refrentie";
            // 
            // txt_Refentie
            // 
            this.txt_Refentie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Refentie.Location = new System.Drawing.Point(18, 27);
            this.txt_Refentie.Multiline = true;
            this.txt_Refentie.Name = "txt_Refentie";
            this.txt_Refentie.Size = new System.Drawing.Size(363, 31);
            this.txt_Refentie.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_stockAanvullen);
            this.tabPage2.Controls.Add(this.btn_nieuwModel);
            this.tabPage2.Controls.Add(this.btn_nieuwMerk);
            this.tabPage2.Controls.Add(this.lv_Inhoud);
            this.tabPage2.Controls.Add(this.tv_Merken);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(842, 526);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Stock";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_stockAanvullen
            // 
            this.btn_stockAanvullen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_stockAanvullen.Location = new System.Drawing.Point(654, 488);
            this.btn_stockAanvullen.Name = "btn_stockAanvullen";
            this.btn_stockAanvullen.Size = new System.Drawing.Size(182, 23);
            this.btn_stockAanvullen.TabIndex = 2;
            this.btn_stockAanvullen.Text = "Stock Aanvullen";
            this.btn_stockAanvullen.UseVisualStyleBackColor = true;
            this.btn_stockAanvullen.Click += new System.EventHandler(this.btn_stockAanvullen_Click);
            // 
            // btn_nieuwModel
            // 
            this.btn_nieuwModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_nieuwModel.Location = new System.Drawing.Point(194, 488);
            this.btn_nieuwModel.Name = "btn_nieuwModel";
            this.btn_nieuwModel.Size = new System.Drawing.Size(182, 23);
            this.btn_nieuwModel.TabIndex = 2;
            this.btn_nieuwModel.Text = "Niew Model";
            this.btn_nieuwModel.UseVisualStyleBackColor = true;
            this.btn_nieuwModel.Click += new System.EventHandler(this.btn_nieuwModel_Click);
            // 
            // btn_nieuwMerk
            // 
            this.btn_nieuwMerk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_nieuwMerk.Location = new System.Drawing.Point(6, 488);
            this.btn_nieuwMerk.Name = "btn_nieuwMerk";
            this.btn_nieuwMerk.Size = new System.Drawing.Size(182, 23);
            this.btn_nieuwMerk.TabIndex = 2;
            this.btn_nieuwMerk.Text = "Niew Merk";
            this.btn_nieuwMerk.UseVisualStyleBackColor = true;
            this.btn_nieuwMerk.Click += new System.EventHandler(this.btn_nieuwMerk_Click);
            // 
            // lv_Inhoud
            // 
            this.lv_Inhoud.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv_Inhoud.Location = new System.Drawing.Point(194, 6);
            this.lv_Inhoud.Name = "lv_Inhoud";
            this.lv_Inhoud.Size = new System.Drawing.Size(642, 466);
            this.lv_Inhoud.TabIndex = 1;
            this.lv_Inhoud.UseCompatibleStateImageBehavior = false;
            this.lv_Inhoud.View = System.Windows.Forms.View.List;
            // 
            // tv_Merken
            // 
            this.tv_Merken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tv_Merken.Location = new System.Drawing.Point(6, 6);
            this.tv_Merken.Name = "tv_Merken";
            this.tv_Merken.Size = new System.Drawing.Size(182, 466);
            this.tv_Merken.TabIndex = 0;
            this.tv_Merken.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Merken_AfterSelect);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lsb_OverzichtDodOfOd);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(842, 526);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Retour";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lsb_OverzichtDodOfOd
            // 
            this.lsb_OverzichtDodOfOd.FormattingEnabled = true;
            this.lsb_OverzichtDodOfOd.Location = new System.Drawing.Point(438, 11);
            this.lsb_OverzichtDodOfOd.Name = "lsb_OverzichtDodOfOd";
            this.lsb_OverzichtDodOfOd.Size = new System.Drawing.Size(346, 459);
            this.lsb_OverzichtDodOfOd.TabIndex = 4;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groep);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(426, 514);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nieuw Inschrijving";
            // 
            // groep
            // 
            this.groep.Controls.Add(this.rdb_OwnDamage);
            this.groep.Controls.Add(this.rdb_reDOA);
            this.groep.Controls.Add(this.btn_reVerwijderen);
            this.groep.Controls.Add(this.btn_reOpslaan);
            this.groep.Controls.Add(this.txt_reInkoopPrijs);
            this.groep.Controls.Add(this.label9);
            this.groep.Controls.Add(this.textBox4);
            this.groep.Controls.Add(this.label10);
            this.groep.Controls.Add(this.txt_reOnderdeelNaam);
            this.groep.Controls.Add(this.label11);
            this.groep.Controls.Add(this.txt_re_Model);
            this.groep.Controls.Add(this.label12);
            this.groep.Controls.Add(this.txt_reMerk);
            this.groep.Controls.Add(this.label13);
            this.groep.Controls.Add(this.cob_retoestel);
            this.groep.Controls.Add(this.label14);
            this.groep.Location = new System.Drawing.Point(6, 123);
            this.groep.Name = "groep";
            this.groep.Size = new System.Drawing.Size(414, 385);
            this.groep.TabIndex = 3;
            this.groep.TabStop = false;
            this.groep.Text = "Gegevens";
            // 
            // rdb_OwnDamage
            // 
            this.rdb_OwnDamage.AutoSize = true;
            this.rdb_OwnDamage.Location = new System.Drawing.Point(105, 289);
            this.rdb_OwnDamage.Name = "rdb_OwnDamage";
            this.rdb_OwnDamage.Size = new System.Drawing.Size(41, 17);
            this.rdb_OwnDamage.TabIndex = 14;
            this.rdb_OwnDamage.TabStop = true;
            this.rdb_OwnDamage.Text = "OD";
            this.rdb_OwnDamage.UseVisualStyleBackColor = true;
            // 
            // rdb_reDOA
            // 
            this.rdb_reDOA.AutoSize = true;
            this.rdb_reDOA.Location = new System.Drawing.Point(105, 266);
            this.rdb_reDOA.Name = "rdb_reDOA";
            this.rdb_reDOA.Size = new System.Drawing.Size(48, 17);
            this.rdb_reDOA.TabIndex = 13;
            this.rdb_reDOA.TabStop = true;
            this.rdb_reDOA.Text = "DOA";
            this.rdb_reDOA.UseVisualStyleBackColor = true;
            // 
            // btn_reVerwijderen
            // 
            this.btn_reVerwijderen.Location = new System.Drawing.Point(262, 349);
            this.btn_reVerwijderen.Name = "btn_reVerwijderen";
            this.btn_reVerwijderen.Size = new System.Drawing.Size(137, 23);
            this.btn_reVerwijderen.TabIndex = 12;
            this.btn_reVerwijderen.Text = "Verwijderd";
            this.btn_reVerwijderen.UseVisualStyleBackColor = true;
            // 
            // btn_reOpslaan
            // 
            this.btn_reOpslaan.Location = new System.Drawing.Point(105, 349);
            this.btn_reOpslaan.Name = "btn_reOpslaan";
            this.btn_reOpslaan.Size = new System.Drawing.Size(137, 23);
            this.btn_reOpslaan.TabIndex = 12;
            this.btn_reOpslaan.Text = "Opslaan";
            this.btn_reOpslaan.UseVisualStyleBackColor = true;
            // 
            // txt_reInkoopPrijs
            // 
            this.txt_reInkoopPrijs.Location = new System.Drawing.Point(105, 222);
            this.txt_reInkoopPrijs.Name = "txt_reInkoopPrijs";
            this.txt_reInkoopPrijs.Size = new System.Drawing.Size(87, 20);
            this.txt_reInkoopPrijs.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "InkoopPrijs";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(105, 196);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(87, 20);
            this.textBox4.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Aantal";
            // 
            // txt_reOnderdeelNaam
            // 
            this.txt_reOnderdeelNaam.Location = new System.Drawing.Point(105, 152);
            this.txt_reOnderdeelNaam.Name = "txt_reOnderdeelNaam";
            this.txt_reOnderdeelNaam.Size = new System.Drawing.Size(294, 20);
            this.txt_reOnderdeelNaam.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 155);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "OnderdeelNaam";
            // 
            // txt_re_Model
            // 
            this.txt_re_Model.Location = new System.Drawing.Point(105, 115);
            this.txt_re_Model.Name = "txt_re_Model";
            this.txt_re_Model.Size = new System.Drawing.Size(294, 20);
            this.txt_re_Model.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Model";
            // 
            // txt_reMerk
            // 
            this.txt_reMerk.Location = new System.Drawing.Point(105, 77);
            this.txt_reMerk.Name = "txt_reMerk";
            this.txt_reMerk.Size = new System.Drawing.Size(294, 20);
            this.txt_reMerk.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Merk";
            // 
            // cob_retoestel
            // 
            this.cob_retoestel.FormattingEnabled = true;
            this.cob_retoestel.Location = new System.Drawing.Point(105, 41);
            this.cob_retoestel.Name = "cob_retoestel";
            this.cob_retoestel.Size = new System.Drawing.Size(294, 21);
            this.cob_retoestel.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Toestel";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txt_reRefrentie);
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(414, 81);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Refrentie";
            // 
            // txt_reRefrentie
            // 
            this.txt_reRefrentie.Location = new System.Drawing.Point(18, 29);
            this.txt_reRefrentie.Multiline = true;
            this.txt_reRefrentie.Name = "txt_reRefrentie";
            this.txt_reRefrentie.Size = new System.Drawing.Size(381, 31);
            this.txt_reRefrentie.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(438, 478);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(346, 36);
            this.button3.TabIndex = 5;
            this.button3.Text = "Export";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // InventorApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 577);
            this.Controls.Add(this.tabControl1);
            this.Name = "InventorApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InventrorApp";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.InventorApp_Load);
            this.tabControl1.ResumeLayout(false);
            this.tp_inschrijven.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gb_gegevens.ResumeLayout(false);
            this.gb_gegevens.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groep.ResumeLayout(false);
            this.groep.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_inschrijven;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gb_gegevens;
        private System.Windows.Forms.TextBox txt_marge;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_InKoopPrijs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_OnderdeelNaam;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Model;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Merk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cob_Toestel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_Refentie;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox lsb_lijstHerstelingen;
        private System.Windows.Forms.Button btn_Opslaan;
        private System.Windows.Forms.Button btn_verwijderen;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.ListView lv_Inhoud;
        private System.Windows.Forms.TreeView tv_Merken;
        private System.Windows.Forms.Button btn_stockAanvullen;
        private System.Windows.Forms.Button btn_nieuwModel;
        private System.Windows.Forms.Button btn_nieuwMerk;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListBox lsb_OverzichtDodOfOd;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groep;
        private System.Windows.Forms.RadioButton rdb_OwnDamage;
        private System.Windows.Forms.RadioButton rdb_reDOA;
        private System.Windows.Forms.Button btn_reVerwijderen;
        private System.Windows.Forms.Button btn_reOpslaan;
        private System.Windows.Forms.TextBox txt_reInkoopPrijs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_reOnderdeelNaam;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_re_Model;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_reMerk;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cob_retoestel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txt_reRefrentie;
        private System.Windows.Forms.Button button3;
    }
}

