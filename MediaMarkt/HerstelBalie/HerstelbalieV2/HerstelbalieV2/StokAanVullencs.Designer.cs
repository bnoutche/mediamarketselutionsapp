﻿namespace HerstelbalieV2
{
    partial class StokAanVullen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_OnderdeelNaam = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Stock = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Toevoegen = new System.Windows.Forms.Button();
            this.Marge = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_InkoopPrijs = new System.Windows.Forms.TextBox();
            this.txt_Marge = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_VerzendKoste = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbo_Lerverancier = new System.Windows.Forms.ComboBox();
            this.txt_Prijs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_option = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_personeel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_min = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_model = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_OnderdeelNaam
            // 
            this.txt_OnderdeelNaam.Location = new System.Drawing.Point(105, 24);
            this.txt_OnderdeelNaam.Name = "txt_OnderdeelNaam";
            this.txt_OnderdeelNaam.Size = new System.Drawing.Size(230, 20);
            this.txt_OnderdeelNaam.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Onderdeel Naam";
            // 
            // txt_Stock
            // 
            this.txt_Stock.Location = new System.Drawing.Point(105, 88);
            this.txt_Stock.Name = "txt_Stock";
            this.txt_Stock.Size = new System.Drawing.Size(230, 20);
            this.txt_Stock.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Stock";
            // 
            // btn_Toevoegen
            // 
            this.btn_Toevoegen.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Toevoegen.Location = new System.Drawing.Point(106, 375);
            this.btn_Toevoegen.Name = "btn_Toevoegen";
            this.btn_Toevoegen.Size = new System.Drawing.Size(230, 51);
            this.btn_Toevoegen.TabIndex = 11;
            this.btn_Toevoegen.Text = "Toevoegen";
            this.btn_Toevoegen.UseVisualStyleBackColor = true;
            // 
            // Marge
            // 
            this.Marge.AutoSize = true;
            this.Marge.Location = new System.Drawing.Point(18, 237);
            this.Marge.Name = "Marge";
            this.Marge.Size = new System.Drawing.Size(37, 13);
            this.Marge.TabIndex = 9;
            this.Marge.Text = "Marge";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Inkoop Prijs";
            // 
            // txt_InkoopPrijs
            // 
            this.txt_InkoopPrijs.Location = new System.Drawing.Point(105, 143);
            this.txt_InkoopPrijs.Name = "txt_InkoopPrijs";
            this.txt_InkoopPrijs.Size = new System.Drawing.Size(100, 20);
            this.txt_InkoopPrijs.TabIndex = 8;
            // 
            // txt_Marge
            // 
            this.txt_Marge.Location = new System.Drawing.Point(105, 234);
            this.txt_Marge.Name = "txt_Marge";
            this.txt_Marge.Size = new System.Drawing.Size(100, 20);
            this.txt_Marge.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "VerzendKoste";
            // 
            // txt_VerzendKoste
            // 
            this.txt_VerzendKoste.Location = new System.Drawing.Point(105, 264);
            this.txt_VerzendKoste.Name = "txt_VerzendKoste";
            this.txt_VerzendKoste.Size = new System.Drawing.Size(100, 20);
            this.txt_VerzendKoste.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Leverancier";
            // 
            // cbo_Lerverancier
            // 
            this.cbo_Lerverancier.FormattingEnabled = true;
            this.cbo_Lerverancier.Location = new System.Drawing.Point(105, 304);
            this.cbo_Lerverancier.Name = "cbo_Lerverancier";
            this.cbo_Lerverancier.Size = new System.Drawing.Size(230, 21);
            this.cbo_Lerverancier.TabIndex = 18;
            // 
            // txt_Prijs
            // 
            this.txt_Prijs.Location = new System.Drawing.Point(105, 171);
            this.txt_Prijs.Name = "txt_Prijs";
            this.txt_Prijs.Size = new System.Drawing.Size(100, 20);
            this.txt_Prijs.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Prijs";
            // 
            // Txt_option
            // 
            this.Txt_option.Location = new System.Drawing.Point(105, 59);
            this.Txt_option.Name = "Txt_option";
            this.Txt_option.Size = new System.Drawing.Size(230, 20);
            this.Txt_option.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Option";
            // 
            // txt_personeel
            // 
            this.txt_personeel.Location = new System.Drawing.Point(105, 202);
            this.txt_personeel.Name = "txt_personeel";
            this.txt_personeel.Size = new System.Drawing.Size(100, 20);
            this.txt_personeel.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Personeel";
            // 
            // txt_min
            // 
            this.txt_min.Location = new System.Drawing.Point(105, 114);
            this.txt_min.Name = "txt_min";
            this.txt_min.Size = new System.Drawing.Size(100, 20);
            this.txt_min.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 119);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Min";
            // 
            // lbl_model
            // 
            this.lbl_model.AutoSize = true;
            this.lbl_model.Location = new System.Drawing.Point(106, 344);
            this.lbl_model.Name = "lbl_model";
            this.lbl_model.Size = new System.Drawing.Size(0, 13);
            this.lbl_model.TabIndex = 27;
            this.lbl_model.Visible = false;
            // 
            // StokAanVullen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 438);
            this.Controls.Add(this.lbl_model);
            this.Controls.Add(this.txt_min);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_personeel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_option);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_Prijs);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbo_Lerverancier);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_VerzendKoste);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_Marge);
            this.Controls.Add(this.txt_InkoopPrijs);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Marge);
            this.Controls.Add(this.btn_Toevoegen);
            this.Controls.Add(this.txt_Stock);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_OnderdeelNaam);
            this.Controls.Add(this.label3);
            this.Name = "StokAanVullen";
            this.Text = "StokAanVullen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_OnderdeelNaam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Stock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Toevoegen;
        private System.Windows.Forms.Label Marge;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_InkoopPrijs;
        private System.Windows.Forms.TextBox txt_Marge;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_VerzendKoste;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbo_Lerverancier;
        private System.Windows.Forms.TextBox txt_Prijs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_option;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_personeel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_min;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_model;
    }
}