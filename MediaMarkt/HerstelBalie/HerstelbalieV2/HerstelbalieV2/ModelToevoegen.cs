﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassInventor;

namespace HerstelbalieV2
{
    public partial class ModelToevoegen : Form
    {
        public Stock Stock
        {
            get
            {
                Stock tmp = new Stock();
                {
                    tmp.Merk = txt_MerkNaam.Text;
                    tmp.Model = txt_ModelNaam.Text;
                }

                return tmp;
            }
        }
        public ModelToevoegen()
        {
            InitializeComponent();
        }
    }
}
