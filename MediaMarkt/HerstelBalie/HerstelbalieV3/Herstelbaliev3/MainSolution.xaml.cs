﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Herstelbaliev3
{
    /// <summary>
    /// Interaction logic for MainSolution.xaml
    /// </summary>
    public partial class MainSolution : Window
    {
        public MainSolution()
        {
            InitializeComponent();
        }

        private void btn_GetStarted_Click(object sender, RoutedEventArgs e)
        {
            WorkFloor wrf = new WorkFloor();
            wrf.ShowDialog();
        }
    }
}
