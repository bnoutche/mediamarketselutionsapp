﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventorApp.Code
{
   public class Stock : Onderdeel
    {
       public int Voorraad { get; set; }
        public event EventHandler stockAanvullen;

        public Stock()
       {
           
       }

       public Stock(int voorRaad)
       {
           this.Voorraad = this.Voorraad;
       }
       public Stock(Onderdeel onderdeel):base(onderdeel)
       {
           this.Voorraad = 3;
       }


       public int StockVerschil(int aantal, int voorraad)
       {
           int verschil = voorraad - aantal;

           this.Voorraad = verschil;

           return this.Voorraad;

       }

        private void StockAanVullen(int stock)
        {

            if (stock < this.Voorraad)
            {

                if (stockAanvullen != null)
                {
                    stockAanvullen(this, EventArgs.Empty);

                }

            }
        }


    }
}
