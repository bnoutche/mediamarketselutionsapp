﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventorApp.Code
{
    interface IOnderdeelMethodes
    {
        Double BtwBerekening();

        Double MargesBerekening();

    }
}
