﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventorApp.Code
{
    public class Onderdeel : IOnderdeelMethodes 
    {
        public int Referentie { get; set; }
        public String OnderdeelNaam { get; set; }
        public String Merk { get; set; }
        public String Model { get; set; }
        public int Aantal { get; set; }
        public String ToestelOnderdeel { get; set; }
        public double InkoopPrijs { get; set; }
        public double Trans { get; set; }




        public Onderdeel()
        {
            this.Aantal = 0;
            this.InkoopPrijs = 0;
            this.Merk = String.Empty;
            this.OnderdeelNaam = String.Empty;
            this.Referentie = 0;
            this.ToestelOnderdeel = String.Empty;
            this.Model = String.Empty;
            this.Trans = 0;

        }



        public Onderdeel(Onderdeel onderdeel)
        {
            this.OnderdeelNaam = onderdeel.OnderdeelNaam;
            this.Referentie = onderdeel.Referentie;
            this.Merk = onderdeel.Merk;
            this.Model = onderdeel.Model;
            this.ToestelOnderdeel = onderdeel.ToestelOnderdeel;
            this.Aantal = onderdeel.Aantal;
            this.InkoopPrijs = onderdeel.InkoopPrijs;
        }

        public Onderdeel(String onderdeelNaam, int referetie, String merk, string model, String toestel, int aantal,
            double inkoopPrijs)
        {
            this.OnderdeelNaam = onderdeelNaam;
            this.Referentie = referetie;
            this.Merk = merk;
            this.Model = model;
            this.ToestelOnderdeel = toestel;
            this.Aantal = aantal;
            this.InkoopPrijs = inkoopPrijs;
        }

       

        public double BtwBerekening()
        {

            double incBtw = this.InkoopPrijs*1.21;
            return incBtw;

        }

        public double MargesBerekening()
        {
            double marge = this.InkoopPrijs*1.21*1.35 + this.Trans;

            return marge;
        }

        public override string ToString()
        {
            return "Toestel: "+ this.ToestelOnderdeel+ " |" + " Model: "+ this.Model.ToString() + " |" +
                   " OnderdeelNaam: " + this.OnderdeelNaam.ToString();
        }
    }
}

