﻿namespace InventorApp
{
    partial class FmNewMerk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_merkNaam = new System.Windows.Forms.TextBox();
            this.btn_Opslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Merk";
            // 
            // txt_merkNaam
            // 
            this.txt_merkNaam.Location = new System.Drawing.Point(114, 36);
            this.txt_merkNaam.Name = "txt_merkNaam";
            this.txt_merkNaam.Size = new System.Drawing.Size(482, 20);
            this.txt_merkNaam.TabIndex = 1;
            // 
            // btn_Opslaan
            // 
            this.btn_Opslaan.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Opslaan.Location = new System.Drawing.Point(430, 62);
            this.btn_Opslaan.Name = "btn_Opslaan";
            this.btn_Opslaan.Size = new System.Drawing.Size(166, 23);
            this.btn_Opslaan.TabIndex = 2;
            this.btn_Opslaan.Text = "Opslaan";
            this.btn_Opslaan.UseVisualStyleBackColor = true;
            this.btn_Opslaan.Click += new System.EventHandler(this.btn_Opslaan_Click);
            // 
            // FmNewMerk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 91);
            this.Controls.Add(this.btn_Opslaan);
            this.Controls.Add(this.txt_merkNaam);
            this.Controls.Add(this.label1);
            this.Name = "FmNewMerk";
            this.Text = "New Merk";
            this.Load += new System.EventHandler(this.fm_NewMerk_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_merkNaam;
        private System.Windows.Forms.Button btn_Opslaan;
    }
}