﻿namespace InventorApp
{
    partial class InventorMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tap_Options = new System.Windows.Forms.TabControl();
            this.tap_Inschijven = new System.Windows.Forms.TabPage();
            this.txt_Aantal = new System.Windows.Forms.TextBox();
            this.txt_InkoopPrijs = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lbl_OverzichtProductie = new System.Windows.Forms.ListBox();
            this.btn_Oplaan = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Merk = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Refentie = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Type = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_Toestel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_OnderdeelNaam = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_Aanvullen = new System.Windows.Forms.Button();
            this.btn_Onderdeel = new System.Windows.Forms.Button();
            this.btn_NewMerk = new System.Windows.Forms.Button();
            this.lv_Onderdelen = new System.Windows.Forms.ListView();
            this.tv_Merken = new System.Windows.Forms.TreeView();
            this.tap_Options.SuspendLayout();
            this.tap_Inschijven.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tap_Options
            // 
            this.tap_Options.Controls.Add(this.tap_Inschijven);
            this.tap_Options.Controls.Add(this.tabPage2);
            this.tap_Options.Location = new System.Drawing.Point(12, 12);
            this.tap_Options.Name = "tap_Options";
            this.tap_Options.SelectedIndex = 0;
            this.tap_Options.Size = new System.Drawing.Size(805, 432);
            this.tap_Options.TabIndex = 0;
            // 
            // tap_Inschijven
            // 
            this.tap_Inschijven.Controls.Add(this.txt_Aantal);
            this.tap_Inschijven.Controls.Add(this.txt_InkoopPrijs);
            this.tap_Inschijven.Controls.Add(this.label7);
            this.tap_Inschijven.Controls.Add(this.lbl_OverzichtProductie);
            this.tap_Inschijven.Controls.Add(this.btn_Oplaan);
            this.tap_Inschijven.Controls.Add(this.label6);
            this.tap_Inschijven.Controls.Add(this.txt_Merk);
            this.tap_Inschijven.Controls.Add(this.label5);
            this.tap_Inschijven.Controls.Add(this.txt_Refentie);
            this.tap_Inschijven.Controls.Add(this.label4);
            this.tap_Inschijven.Controls.Add(this.txt_Type);
            this.tap_Inschijven.Controls.Add(this.label3);
            this.tap_Inschijven.Controls.Add(this.cbo_Toestel);
            this.tap_Inschijven.Controls.Add(this.label2);
            this.tap_Inschijven.Controls.Add(this.label1);
            this.tap_Inschijven.Controls.Add(this.txt_OnderdeelNaam);
            this.tap_Inschijven.Location = new System.Drawing.Point(4, 22);
            this.tap_Inschijven.Name = "tap_Inschijven";
            this.tap_Inschijven.Padding = new System.Windows.Forms.Padding(3);
            this.tap_Inschijven.Size = new System.Drawing.Size(797, 406);
            this.tap_Inschijven.TabIndex = 0;
            this.tap_Inschijven.Text = "Inschrijven";
            this.tap_Inschijven.UseVisualStyleBackColor = true;
            // 
            // txt_Aantal
            // 
            this.txt_Aantal.Location = new System.Drawing.Point(96, 199);
            this.txt_Aantal.Name = "txt_Aantal";
            this.txt_Aantal.Size = new System.Drawing.Size(176, 20);
            this.txt_Aantal.TabIndex = 6;
            // 
            // txt_InkoopPrijs
            // 
            this.txt_InkoopPrijs.Location = new System.Drawing.Point(96, 230);
            this.txt_InkoopPrijs.Name = "txt_InkoopPrijs";
            this.txt_InkoopPrijs.Size = new System.Drawing.Size(176, 20);
            this.txt_InkoopPrijs.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Inkoopprijs";
            // 
            // lbl_OverzichtProductie
            // 
            this.lbl_OverzichtProductie.FormattingEnabled = true;
            this.lbl_OverzichtProductie.Location = new System.Drawing.Point(372, 44);
            this.lbl_OverzichtProductie.Name = "lbl_OverzichtProductie";
            this.lbl_OverzichtProductie.Size = new System.Drawing.Size(400, 316);
            this.lbl_OverzichtProductie.TabIndex = 14;
            // 
            // btn_Oplaan
            // 
            this.btn_Oplaan.Location = new System.Drawing.Point(96, 284);
            this.btn_Oplaan.Name = "btn_Oplaan";
            this.btn_Oplaan.Size = new System.Drawing.Size(176, 75);
            this.btn_Oplaan.TabIndex = 8;
            this.btn_Oplaan.Text = "Opslaan";
            this.btn_Oplaan.UseVisualStyleBackColor = true;
            this.btn_Oplaan.Click += new System.EventHandler(this.btn_Oplaan_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Aantal";
            // 
            // txt_Merk
            // 
            this.txt_Merk.Location = new System.Drawing.Point(96, 170);
            this.txt_Merk.Name = "txt_Merk";
            this.txt_Merk.Size = new System.Drawing.Size(176, 20);
            this.txt_Merk.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Merk";
            // 
            // txt_Refentie
            // 
            this.txt_Refentie.Location = new System.Drawing.Point(96, 44);
            this.txt_Refentie.Name = "txt_Refentie";
            this.txt_Refentie.Size = new System.Drawing.Size(176, 20);
            this.txt_Refentie.TabIndex = 1;
            this.txt_Refentie.TextChanged += new System.EventHandler(this.txt_Refentie_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Referentie";
            // 
            // txt_Type
            // 
            this.txt_Type.Location = new System.Drawing.Point(96, 137);
            this.txt_Type.Name = "txt_Type";
            this.txt_Type.Size = new System.Drawing.Size(176, 20);
            this.txt_Type.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Model";
            // 
            // cbo_Toestel
            // 
            this.cbo_Toestel.FormattingEnabled = true;
            this.cbo_Toestel.Location = new System.Drawing.Point(96, 105);
            this.cbo_Toestel.Name = "cbo_Toestel";
            this.cbo_Toestel.Size = new System.Drawing.Size(176, 21);
            this.cbo_Toestel.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Toestel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "OnderdeelNaam";
            // 
            // txt_OnderdeelNaam
            // 
            this.txt_OnderdeelNaam.Location = new System.Drawing.Point(96, 75);
            this.txt_OnderdeelNaam.Name = "txt_OnderdeelNaam";
            this.txt_OnderdeelNaam.Size = new System.Drawing.Size(176, 20);
            this.txt_OnderdeelNaam.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_Aanvullen);
            this.tabPage2.Controls.Add(this.btn_Onderdeel);
            this.tabPage2.Controls.Add(this.btn_NewMerk);
            this.tabPage2.Controls.Add(this.lv_Onderdelen);
            this.tabPage2.Controls.Add(this.tv_Merken);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(797, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Stock";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_Aanvullen
            // 
            this.btn_Aanvullen.Location = new System.Drawing.Point(569, 333);
            this.btn_Aanvullen.Name = "btn_Aanvullen";
            this.btn_Aanvullen.Size = new System.Drawing.Size(222, 66);
            this.btn_Aanvullen.TabIndex = 4;
            this.btn_Aanvullen.Text = "Aanvullen";
            this.btn_Aanvullen.UseVisualStyleBackColor = true;
            // 
            // btn_Onderdeel
            // 
            this.btn_Onderdeel.Location = new System.Drawing.Point(289, 333);
            this.btn_Onderdeel.Name = "btn_Onderdeel";
            this.btn_Onderdeel.Size = new System.Drawing.Size(222, 66);
            this.btn_Onderdeel.TabIndex = 3;
            this.btn_Onderdeel.Text = "New Onderdeel";
            this.btn_Onderdeel.UseVisualStyleBackColor = true;
            // 
            // btn_NewMerk
            // 
            this.btn_NewMerk.Location = new System.Drawing.Point(7, 334);
            this.btn_NewMerk.Name = "btn_NewMerk";
            this.btn_NewMerk.Size = new System.Drawing.Size(222, 66);
            this.btn_NewMerk.TabIndex = 2;
            this.btn_NewMerk.Text = "New Merk";
            this.btn_NewMerk.UseVisualStyleBackColor = true;
            this.btn_NewMerk.Click += new System.EventHandler(this.btn_NewMerk_Click);
            // 
            // lv_Onderdelen
            // 
            this.lv_Onderdelen.Location = new System.Drawing.Point(235, 6);
            this.lv_Onderdelen.Name = "lv_Onderdelen";
            this.lv_Onderdelen.Size = new System.Drawing.Size(556, 321);
            this.lv_Onderdelen.TabIndex = 1;
            this.lv_Onderdelen.UseCompatibleStateImageBehavior = false;
            // 
            // tv_Merken
            // 
            this.tv_Merken.Location = new System.Drawing.Point(6, 6);
            this.tv_Merken.Name = "tv_Merken";
            this.tv_Merken.Size = new System.Drawing.Size(223, 321);
            this.tv_Merken.TabIndex = 0;
            this.tv_Merken.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Merken_AfterSelect);
            // 
            // InventorMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 456);
            this.Controls.Add(this.tap_Options);
            this.Name = "InventorMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventor ";
            this.tap_Options.ResumeLayout(false);
            this.tap_Inschijven.ResumeLayout(false);
            this.tap_Inschijven.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tap_Options;
        private System.Windows.Forms.TabPage tap_Inschijven;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_OnderdeelNaam;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_Toestel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Refentie;
        private System.Windows.Forms.TextBox txt_Merk;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox lbl_OverzichtProductie;
        private System.Windows.Forms.Button btn_Oplaan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_InkoopPrijs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_Aantal;
        private System.Windows.Forms.TreeView tv_Merken;
        private System.Windows.Forms.ListView lv_Onderdelen;
        private System.Windows.Forms.Button btn_Aanvullen;
        private System.Windows.Forms.Button btn_Onderdeel;
        private System.Windows.Forms.Button btn_NewMerk;
    }
}

