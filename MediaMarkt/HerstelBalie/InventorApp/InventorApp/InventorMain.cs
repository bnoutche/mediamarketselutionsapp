﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InventorApp.Code;

namespace InventorApp
{
    public partial class InventorMain : Form
    {
        private List<Code.Onderdeel> lijstOverzicht;
        public static string sendtext = "";

        
        
        public InventorMain()
        {
            InitializeComponent();
            lijstOverzicht = new List<Code.Onderdeel>();
            VulBox();
            cbo_Toestel.SelectedIndex = 0;
            VulMerken();
            Modeltoevoegen();

            try
            {
                Code.Stock stockVoorRaad = new Stock();
                stockVoorRaad.stockAanvullen += stockVoorRaad.stockAanvullen;
                houderBankrekening.RekeningNegatief += houderBankrekening_RekeningNegatief;
                _afhalen = new Afhalen(houderBankrekening);
                if (_afhalen.ShowDialog() == DialogResult.OK)
                {

                    _rekeninghouderLijst.Remove(houderBankrekening);
                    _rekeninghouderLijst.Add(_afhalen.Bankrekening);

                    VulListBox();

                }
                houderBankrekening.PincondeFoutief -= houderBankrekening_PincondeFoutief;
                houderBankrekening.RekeningNegatief -= houderBankrekening_RekeningNegatief;

            }
            catch (NullReferenceException)
            {
                return;
            }


        }


        public void VulBox()
        {
            cbo_Toestel.Items.Add(Code.Soort.Laptop);
            cbo_Toestel.Items.Add(Code.Soort.SmartPhone);
            cbo_Toestel.Items.Add(Code.Soort.Tablet);
        }

        public void VulMerken()
        {

            TreeNode werelddelen = new TreeNode("Merken");
            tv_Merken.Nodes.Add(werelddelen);

            TreeNode Apple = new TreeNode("Apple");

            TreeNode Samsung = new TreeNode("Samsung");
            TreeNode Huawei = new TreeNode("Huawei");
            TreeNode Sony = new TreeNode("Sony");


            tv_Merken.Nodes[0].Nodes.Add(Apple);
            tv_Merken.Nodes[0].Nodes.Add(Samsung);
            tv_Merken.Nodes[0].Nodes.Add(Huawei);
            tv_Merken.Nodes[0].Nodes.Add(Sony);
  
            tv_Merken.ExpandAll();

        }


        private void txt_Refentie_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Oplaan_Click(object sender, EventArgs e)
        {


            Code.Onderdeel onderdeel = new Code.Onderdeel(txt_OnderdeelNaam.Text,Convert.ToInt32(txt_Refentie.Text),
                txt_Merk.Text, txt_Type.Text, cbo_Toestel.SelectedItem.ToString(), Convert.ToInt32(txt_Aantal.Text), Convert.ToDouble(txt_InkoopPrijs.Text));

            lijstOverzicht.Add(onderdeel);
            lbl_OverzichtProductie.Items.Clear();
            foreach (var itm in lijstOverzicht)
            {
              
                lbl_OverzichtProductie.Items.Add(itm);
               
            }
      
        }

        private void btn_NewMerk_Click(object sender, EventArgs e)
        {
            

          ControleBestaandeMerk();


        }

        public void ControleBestaandeMerk()
        {
            FmNewMerk newMerkVenster = new FmNewMerk();
            newMerkVenster.ShowDialog();
            
            
            if (newMerkVenster.DialogResult == DialogResult.OK)
            {

                    if (DepartmentNodeExists(sendtext)==true)
                    {

                    TreeNode newMerk = new TreeNode(sendtext);

                    tv_Merken.Nodes[0].Nodes.Add(newMerk);
                  
                    }
                else
                {
                    MessageBox.Show("Merk Bestaat Al!", "Merken", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    newMerkVenster.ShowDialog();
                    return;
                }

            }


    
        }

        private bool DepartmentNodeExists(string name)
        {
            if (tv_Merken.Nodes[0].Name.Contains(name))
            {
                return true;
            }

            return false;
        }


        public void Modeltoevoegen()
        {

            try
            {
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 4");
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 4s");
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 5");
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 5s");
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 6");
                tv_Merken.Nodes[0].Nodes[0].Nodes.Add("Iphone 6 Plus");

                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy s4 mini");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy S4");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy s5 mini");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy S5");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy s6");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy S6 egde");
                tv_Merken.Nodes[0].Nodes[1].Nodes.Add("Galaxy s6 egde plus");
        

            }

            catch (Exception)
            {


            }

            tv_Merken.Enabled = true;
  



        }

        public void GetValue()
        {
            int nodes = tv_Merken.SelectedNode.Level;
            string naam = tv_Merken.SelectedNode.Text;

            if (nodes == 0 || nodes == 1)
            {
                lv_Onderdelen.Items.Clear();
                lv_Onderdelen.Columns.Clear();
            }

            else if (naam == "Iphone 4")
            {
                ListViewItem iphone4 = null;
                lv_Onderdelen.Items.Clear();
                lv_Onderdelen.Columns.Clear();
                lv_Onderdelen.View = View.Details;
                lv_Onderdelen.Columns.Add("Onderdeel", 150, HorizontalAlignment.Left);
                lv_Onderdelen.Columns.Add("Stock", 100, HorizontalAlignment.Center);
                lv_Onderdelen.Columns.Add("Ik", 100, HorizontalAlignment.Center);
                lv_Onderdelen.Columns.Add("Prijs", 100, HorizontalAlignment.Center);
                lv_Onderdelen.Columns.Add("Personeels", 100, HorizontalAlignment.Center);


                iphone4 = lv_Onderdelen.Items.Add("Display BLK");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("Display WHT");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");


                iphone4 = lv_Onderdelen.Items.Add("BackCover BLK");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("BackCover WHT");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("Dockconector BLK");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("Dockconector WHT");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");


                iphone4 = lv_Onderdelen.Items.Add("HomeKnop BLK");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("HomeKnop WHT");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

                iphone4 = lv_Onderdelen.Items.Add("Batterij");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");


                iphone4 = lv_Onderdelen.Items.Add("Pwr,Sens,Earsp");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");


                iphone4 = lv_Onderdelen.Items.Add("Volume, Aux");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");


                iphone4 = lv_Onderdelen.Items.Add("Pwr,Sens,Earsp");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("8");
                iphone4.SubItems.Add("49");
                iphone4.SubItems.Add("13.89");
                iphone4.SubItems.Add("20");

            }
        }

        private void tv_Merken_AfterSelect(object sender, TreeViewEventArgs e)
        {
            GetValue();
        }
    }
    }

