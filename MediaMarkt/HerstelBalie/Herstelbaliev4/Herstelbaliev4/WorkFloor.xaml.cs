﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClassInventor;
using ClassInventor.Dal;
using ClassInventor.Model;

namespace Herstelbaliev4
{
    /// <summary>
    /// Interaction logic for WorkFloor.xaml
    /// </summary>
    public partial class WorkFloor : Window
    {

        private OnderdeelDal _listOnderdeelDal;
        private Onderdeel _onderdeel;
        private ObservableCollection<Onderdeel> _onderListstock;
        private ObservableCollection<HardWare> _hardwerList;
        private ObservableCollection<HardWare> _filter;



        public WorkFloor()
        {
            InitializeComponent();
            Localstock();
            FilterModel();
            GetStockList();
            FilterOnBrand();


        }

        public void Localstock()
        {
            _onderListstock = new ObservableCollection<Onderdeel>();
            _hardwerList = new ObservableCollection<HardWare>();
            _onderdeel = new Onderdeel();

            _listOnderdeelDal = new OnderdeelDal(_onderdeel);

            foreach (var itm in _listOnderdeelDal.ReadAll())
            {

                _onderListstock.Add(itm);
            }

            foreach (var dev in _listOnderdeelDal.GethardWareList())
            {

                _hardwerList.Add(dev);
            }

        }


        public void GetStockList()
        {
            foreach (var itm in _onderListstock)
            {

                foreach (var hd in _hardwerList)
                {
                    if (itm.HardWareId == hd.IdHardware)
                    {
                        lv_devices.Items.Add(" Group: " + hd.Model + Environment.NewLine + " " + itm.ToString());
                    }

                }

            }
        }
        private void btn_ModelToeVoegen_Click(object sender, RoutedEventArgs e)
        {
            Newdevice nwd = new Newdevice();
            nwd.ShowDialog();

            if (nwd.DialogResult.HasValue && nwd.DialogResult.Value)
            {

                HardWare model = new HardWare();
                model.Merk = nwd.NewDevice.Merk;
                model.Model = nwd.NewDevice.Model;
                model.Type = nwd.NewDevice.Type;
                HardWareDal dal = new HardWareDal(model);
                dal.Create();

                System.Windows.MessageBox.Show(dal.Message, "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void FilterModel()
        {
            Onderdeel model = new Onderdeel();
            OnderdeelDal devices = new OnderdeelDal(model);
            cbo_model.Items.Add("All");
            foreach (var itm in devices.GethardWareList())
            {
                if (cbo_model.Items.Contains(itm.Model))
                {

                }
                else
                {
                    cbo_model.Items.Add(itm.Model);
                }
            }

        }

        private void FilterOnBrand()
        {
            Onderdeel model = new Onderdeel();
            OnderdeelDal devices = new OnderdeelDal(model);
            cbo_Brand.Items.Add("All");
            foreach (var itm in devices.GethardWareList())
            {
                if (cbo_Brand.Items.Contains(itm.Merk))
                {

                }
                else
                {
                    cbo_Brand.Items.Add(itm.Merk);
                }
            }
        }

        public void AddpartsAuto()
        {
            Stock st = new Stock();
            Onderdeel model = new Onderdeel();
            OnderdeelDal devices = new OnderdeelDal(model);
            foreach (var itm in st.GetList())
            {
                model.Naam = itm.OnderdeelNaam;
                model.Option = itm.Color;
                model.Leverancier = itm.Leverancier;
                model.MinVoorRaad = itm.Min;
                model.VoorRaad = itm.VoorRaad;
                model.PrijsKant = (float)itm.Prijs;
                model.IkPrijs = (float)itm.InkoopPrijs;
                model.Trans = (float)itm.VerzendKoste;
                model.Marge = (float)itm.Marge;
                model.PrijsPersoneel = (float)itm.Persooneels;

                foreach (var dv in devices.GethardWareList())
                {

                    if (itm.Model.ToUpper() == dv.Model.ToUpper())
                    {
                        model.HardWareId = dv.IdHardware;

                    }

                }
                devices.Create();
            }

        }
        private void btn_DevicePart_Click(object sender, RoutedEventArgs e)
        {
            AddpartsAuto();

            //AddParts ap = new AddParts();

            //ap.ShowDialog();

            //if (ap.DialogResult.HasValue && ap.DialogResult.Value)
            //{
            //    Onderdeel model = new Onderdeel();
            //    OnderdeelDal dal = new OnderdeelDal(model);
            //    model.Naam = ap.NewOnderdeel.Naam;
            //    model.Option = ap.NewOnderdeel.Option;
            //    model.Leverancier = ap.NewOnderdeel.Leverancier;
            //    model.MinVoorRaad = ap.NewOnderdeel.MinVoorRaad;
            //    model.VoorRaad = ap.NewOnderdeel.VoorRaad;
            //    model.PrijsKant = ap.NewOnderdeel.PrijsKant;
            //    model.IkPrijs = ap.NewOnderdeel.IkPrijs;
            //    model.Trans = ap.NewOnderdeel.Trans;
            //    model.PrijsPersoneel = ap.NewOnderdeel.PrijsPersoneel;
            //    model.HardWareId = ap.NewOnderdeel.HardWareId;
            //    dal.Create();
            //    Localstock();

            //}
        }

        private void cbo_model_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterModelPart();
        }

        private void FilterModelPart()
        {
        

            lv_devices.Items.Clear();

            _filter = new ObservableCollection<HardWare>();

            String selection = cbo_model.SelectedItem.ToString();


            if (selection != "All")
            {
                foreach (var hd in _hardwerList)
                {
                    if (selection.ToUpper() == hd.Model.ToUpper())
                    {
                        _filter.Add(hd);
                    }
                }

                foreach (var itm in _onderListstock)
                {
                    foreach (var hd in _filter)
                    {
                        if (itm.HardWareId == hd.IdHardware)
                        {
                            lv_devices.Items.Add(" Group: " + hd.Model + Environment.NewLine + " " + itm.ToString());
                        }

                    }
                }
            }
            else
            {
                GetStockList();
            }
          
        }

        private void FilterBrand()
        {
            lv_devices.Items.Clear();

            _filter = new ObservableCollection<HardWare>();

            String selection = cbo_Brand.SelectedItem.ToString();


            if (selection != "All")
            {
                foreach (var hd in _hardwerList)
                {
                    if (selection.ToUpper() == hd.Merk.ToUpper())
                    {
                        _filter.Add(hd);
                    }
                }

                foreach (var itm in _onderListstock)
                {
                    foreach (var hd in _filter)
                    {
                        if (itm.HardWareId == hd.IdHardware)
                        {
                            lv_devices.Items.Add(" Group: " + hd.Model + Environment.NewLine + " " + itm.ToString());
                        }

                    }
                }
            }
            else
            {
                GetStockList();
            }

        }


        private void cbo_Brand_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterBrand();
        }

        private void btn_register_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            Onderdeel selectedOnderdeel = (Onderdeel) lv_devices.SelectedItem;

                AddParts ap = new AddParts();

                ap.ShowDialog();

                if (ap.DialogResult.HasValue && ap.DialogResult.Value)
                {
                    Onderdeel model = new Onderdeel();
                    model.Naam = ap.NewOnderdeel.Naam;
                    model.Option = ap.NewOnderdeel.Option;
                    model.Leverancier = ap.NewOnderdeel.Leverancier;
                    model.MinVoorRaad = ap.NewOnderdeel.MinVoorRaad;
                    model.VoorRaad = ap.NewOnderdeel.VoorRaad;
                    model.PrijsKant = ap.NewOnderdeel.PrijsKant;
                    model.IkPrijs = ap.NewOnderdeel.IkPrijs;
                    model.Trans = ap.NewOnderdeel.Trans;
                    model.PrijsPersoneel = ap.NewOnderdeel.PrijsPersoneel;
                    model.HardWareId = ap.NewOnderdeel.HardWareId;
                }
            }
        }
    }

