﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClassInventor;
using ClassInventor.Dal;
using ClassInventor.Model;

namespace Herstelbaliev4
{
    /// <summary>
    /// Interaction logic for Newdevice.xaml
    /// </summary>
    public partial class Newdevice : Window
    {

        private ObservableCollection<HardWare> _toestellen;
        public HardWare NewDevice
        {
            get
            {
                var tmp = new HardWare();
                tmp.Merk = txt_Brand.Text;
                tmp.Model = txt_Model.Text;
                tmp.Type = cbo_type.SelectedValue.ToString();
                return tmp;
            }
        }

        public Newdevice()
        {
            InitializeComponent();
            ListDevicesType();
            AddDevicesTypeToComboBox();

        }

        public void AddDevicesTypeToComboBox()
        {
            foreach (var itm in _toestellen)
            {
                if (!cbo_type.Items.Contains(itm.Merk))
                {
                    cbo_type.Items.Add(itm.Merk);
                }
            }

        }

        public void ListDevicesType()
        {
            HardWare model = new HardWare();

            HardWareDal hdd = new HardWareDal(model);

            _toestellen = new ObservableCollection<HardWare>();
            foreach (var itm in hdd.ReadAll() )
            {
                _toestellen.Add(itm);
            }



        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
