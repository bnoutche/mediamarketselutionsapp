﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClassInventor;
using ClassInventor.Dal;
using ClassInventor.Model;

namespace Herstelbaliev4
{
    /// <summary>
    /// Interaction logic for AddParts.xaml
    /// </summary>
    public partial class AddParts : Window
    {
        private OnderdeelDal _dal;
        private Onderdeel _model;

        public Onderdeel NewOnderdeel
        {

            get
            {
                var tmp = new Onderdeel
                {
                    Naam = txt_Name.Text,
                    Option = txt_option.Text,
                    Leverancier = txt_Supplier.Text,
                    PrijsKant = float.Parse(txt_Price.Text),
                    MinVoorRaad = int.Parse(txt_minimalStock.Text),
                    VoorRaad = int.Parse(txt_Stock.Text),
                    IkPrijs = float.Parse(txt_IkPrice.Text),
                    Marge = float.Parse(txt_Marge.Text)
                };


                foreach (HardWare itm in _dal.GethardWareList())
                {
                    if (itm.Model.ToUpper() == (string)cbo_Device.SelectedValue)
                    {
                        tmp.HardWareId = itm.IdHardware;
                    }
                }

                return tmp;
            }
        }

        public Onderdeel UpDateOnderdeel
        {
            get
            {

                var tmp = new Onderdeel();
                tmp.HardWareId = (int)cbo_Device.SelectedValue;
                tmp.Naam = txt_Name.Text;
                tmp.Option = txt_option.Text;
                tmp.Leverancier = txt_Supplier.Text;
                tmp.PrijsKant = float.Parse(txt_Price.Text);
                tmp.MinVoorRaad = int.Parse(txt_minimalStock.Text);
                tmp.VoorRaad = int.Parse(txt_Stock.Text);
                tmp.IkPrijs = float.Parse(txt_IkPrice.Text);
                tmp.Marge = float.Parse(txt_Marge.Text);

                return tmp;
            }
            set
            {
                _model = value;
               

              
            }
        }


        public AddParts()
        {
            InitializeComponent();
            DevicesList();
            cbo_Device.SelectedIndex = 0;
            cbo_Device_Model.SelectedIndex = 0;
        }


        public AddParts(Onderdeel selectedOnderdeel) : this()
        {
            _model = selectedOnderdeel;
            cbo_Device.SelectedIndex = 0;
            cbo_Device_Model.SelectedIndex = 0;
        }

        public void DevicesList()
        {
            _model = new Onderdeel();
            _dal = new OnderdeelDal(_model);

            foreach (HardWare item in _dal.GethardWareList())
            {
                cbo_Device.Items.Add(item.Model.ToUpper());

            }


        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }



    }


}


