﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Herstelbaliev4
{
    /// <summary>
    /// Interaction logic for MainSelution.xaml
    /// </summary>
    public partial class MainSelution : Window
    {
        public MainSelution()
        {
            InitializeComponent();
        }

        private void btn_GetStarted_Click(object sender, RoutedEventArgs e)
        {
            WorkFloor wrf = new WorkFloor();
            wrf.ShowDialog();
        }
    }
}
