﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventorApp.Models;

namespace InventorApp.Controllers
{
    public class DeviceController : Controller
    {
        // GET: Device

        private ActionsMethodes _aM = new ActionsMethodes();



        [HttpGet]
        public ActionResult CreateNewDevice()
        {
 
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewDevice( HardWare hardWare)
        {
            if (ModelState.IsValid)
            {
                _aM.CreateDevice(hardWare);
               
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            if (id != 0)
            {
                
                return View(_aM.UpdateDevice(id));
            }
          return View();
       
        }

  
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                _aM.DeleteDevice(id);
                return RedirectToAction("ListDevices");

            }
            return View();

        }

        [HttpPost]
        public ActionResult Update(HardWare hardWare)
        {
            if (ModelState.IsValid)
            {
                _aM.UpdateDevice(hardWare);
               return RedirectToAction("ListDevices");
            }
            return View();

        }

        public ActionResult ListDevices()
        {
            return View(_aM.GetAllHarware());
        }


        public ActionResult Details(int id)
        {
            return View(_aM.GetHardware(id));
        }

    }
}