﻿using System.Web.Mvc;
using InventorApp.Models;

namespace InventorApp.Controllers
{


    public class StockController : Controller
    {
        private ActionsMethodes _aM = new ActionsMethodes();

        [HttpGet]
        public ActionResult ListStock()
        {

            return View(_aM.GetAllStock());
        }

        [HttpGet]
        public ActionResult CreateNewStock()
        {
            ViewData["Merken"] = new SelectList(_aM.ListMerken());
            ViewData["Types"] = new SelectList(_aM.ListType());
            ViewBag.HardWareId = new SelectList(_aM.GetAllHarware(), "Id", "Model");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewStock(Stock newStock)
        {
            var prijsIncleBtw = newStock.IkPrijs*1.21;
            var prijsMetMarge = prijsIncleBtw/100*newStock.Marge;
            var totaalPrijs = prijsIncleBtw + prijsMetMarge;
            newStock.Prijs = (float) totaalPrijs;
            newStock.PersoneelsPrijs = (float) (newStock.IkPrijs*1.21*1.12);


            if (ModelState.IsValid)
            {
                HardWare hardWare = _aM.GetHardware(newStock.HardWareId);
                newStock.HardWare = hardWare;
                _aM.CreateStock(newStock);
                ViewData["Merken"] = new SelectList(_aM.ListMerken());
                ViewData["Types"] = new SelectList(_aM.ListType());
                ViewBag.HardWareId = new SelectList(_aM.GetAllHarware(), "Id", "Model");
                return RedirectToAction("Index", "Home");
            }
            return View(newStock);
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            if (id != 0)
            {
                Stock stock = _aM.UpdateStock(id);

                ViewData["Merken"] = new SelectList(_aM.ListMerken(), selectedValue: stock.HardWare.Merk);
                ViewData["Types"] = new SelectList(_aM.ListType(), selectedValue: stock.HardWare.Type);
                ViewBag.HardWareId = new SelectList(_aM.GetAllHarware(), "Id", "Model", selectedValue: stock.HardWare.Id);

                return View(stock);
            }

            return View();
        }

        [HttpPost]
        public ActionResult Update(Stock stock)
        {
            if (ModelState.IsValid)
            {

                _aM.UpdateStock(stock);

                return RedirectToAction("ListStock", "Stock");
            }


            return View(stock);
        }

        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                _aM.DeleteStock(id);
                return RedirectToAction("ListStock");

            }
            return View();



        }


    }
}