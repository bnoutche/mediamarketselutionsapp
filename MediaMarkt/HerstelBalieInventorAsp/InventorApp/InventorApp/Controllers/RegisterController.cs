﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventorApp.Models;

namespace InventorApp.Controllers
{
    public class RegisterController : Controller
    {
        private ActionsMethodes _aM = new ActionsMethodes();

        
        // GET: Register
        public ActionResult Register()
        {

            ViewBag.DateString = _aM.GetDate();

    
            ViewData["RegisterDev"] = _aM.GetAllStock();
            ViewData["Status"] = new SelectList(_aM.GetEnumList(), "Status");
           
            return View();
        }

        [HttpPost]
        public ActionResult Register(Inschrijven inschrijven, int id)
        {
        
            inschrijven.StockId = id;
            if (inschrijven != null)
            {
                ViewData["Status"] = new SelectList(_aM.GetEnumList(), "Status");
                _aM.CreateinInschrijven(inschrijven);
                ViewData["RegisterDev"] = _aM.GetAllStock();
               
                return View();
            }

            return View();

        }

        public ActionResult DoasList()
        {
            return View(_aM.Doas());
        }

    }
}