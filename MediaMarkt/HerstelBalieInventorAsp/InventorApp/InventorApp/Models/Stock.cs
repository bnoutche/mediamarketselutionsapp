﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InventorApp.Models
{
    [Table("Stock", Schema = "dbo")]
    public class Stock
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public String Naam { get; set; }


        public String Option { get; set; }


        [Required]
        public String Leverancier { get; set; }


        [Required]
        public int MinVoorRaad { get; set; }


        [Required]
        public int VoorRaad { get; set; }


        [Required]
        public float IkPrijs { get; set; }

        [Required]
        public float Prijs { get; set; }

        public float TransprotKoste { get; set; }
        public float Marge { get; set; }
        public float PersoneelsPrijs { get; set; }

        [ForeignKey("HardWare")]
        public int HardWareId { get; set; }
        public virtual HardWare HardWare { get; set; }

      

    }
}