﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace InventorApp.Models
{
    [Table("Inschrijven", Schema = "dbo")]
    public class Inschrijven
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int RefNum { get; set; }

        [Required]
        public int Aantal { get; set; }

        public string Status { get; set; }

        public bool Doa { get; set; }
        public bool Retour { get; set; }
        public bool EigenSchade { get; set; }






        [ForeignKey("Stock")]
        public int StockId { get; set; }
        public virtual Stock Stock { get; set; }

    }
}