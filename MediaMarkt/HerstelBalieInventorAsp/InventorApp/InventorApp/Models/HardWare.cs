﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InventorApp.Models
{
    [Table("HardWare", Schema = "dbo")]
    public class HardWare
    {
        [Key]
        public int Id { get; set; }
        public String Type { get; set; }
        public String Merk { get; set; }
        public String Model { get; set; }


    }
}