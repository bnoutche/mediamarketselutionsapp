﻿using System.Data.Entity;

namespace InventorApp.Models
{
    public class InventorEntities:DbContext
    {
        public InventorEntities()
     : base("name=InventorEntities")
        {

        }

        public DbSet<Stock> Stock { get; set; }
        public DbSet<HardWare> HardWare { get; set; }
        public DbSet<Inschrijven> Inschrijven { get; set; }
 



    }
}