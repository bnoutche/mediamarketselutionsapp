﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventorApp.Models
{
    public class VoegOnderdelenToe
    {
        private InventorEntities db = new InventorEntities();

        public void VoegToenStock()
        {
            var listhardware = db.HardWare.ToList();

            CsvConverter st = new CsvConverter();
            Stock model = new Stock();

            foreach (var itm in st.GetList())
            {
                model.Naam = itm.Naam;
                model.Option = itm.Option;
                model.Leverancier = itm.Leverancier;
                model.MinVoorRaad = itm.MinVoorRaad;
                model.VoorRaad = itm.VoorRaad;
                model.Prijs = (float) itm.Prijs;
                model.IkPrijs = (float) itm.IkPrijs;
                model.TransprotKoste = (float) itm.TransprotKoste;
                model.Marge = (float) itm.Marge;
                model.PersoneelsPrijs = (float) itm.PersoneelsPrijs;

                foreach (var hd in listhardware)
                {
                    if (itm.HardWare.Model == hd.Model)
                    {
                        model.HardWareId = hd.Id;

                        db.Stock.Add(model);
                        db.SaveChanges();
                    }

                }

            }

        }

        public void VoegToenhardware()
        {
            CsvConverter st = new CsvConverter();
            HardWare model = new HardWare();
            foreach (var itm in st.GetHardwareList())
            {
                var modelexe = db.HardWare.FirstOrDefault(a => a.Model == itm.Model);
                if (modelexe == null)
                {
                    model.Model = itm.Model;
                    model.Merk = itm.Merk;
                    model.Type = itm.Type;
                    db.HardWare.Add(model);
                    db.SaveChanges();

                }

            }
        }
    }
}