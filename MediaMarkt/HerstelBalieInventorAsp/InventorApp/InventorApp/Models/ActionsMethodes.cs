﻿using InventorApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace InventorApp.Models
{

    public class ActionsMethodes
    {
        private  List<string> statuslist = new List<string>();

      private  InventorEntities _db = new InventorEntities();
        public List<Stock> GetAllStock()
        {
            var stock = _db.Stock.Include(h => h.HardWare).ToList();

            return stock;
        }
        public List<HardWare> GetAllHarware()
        {
            var hardware = _db.HardWare.ToList();
            return hardware;
        }

        public HardWare GetHardware(int id)
        {
            var findHardware = _db.HardWare.Find(id);
            return findHardware;
        }


        public HardWare CreateDevice(HardWare hardWare)
        {
           
            if (hardWare != null)
            {
                HardWare hd = new HardWare();
                hd.Merk = hardWare.Merk.ToUpper();
                hd.Model = hardWare.Model.ToUpper();
                hd.Type = hardWare.Type.ToUpper();
              
                _db.HardWare.Add(hd);
                _db.SaveChanges();
            }

            return hardWare;

        }

        public HardWare UpdateDevice(int id)
        {
            var upateHardware = _db.HardWare.Find(id);
           

            return upateHardware;

        }

        public void DeleteDevice(int id)
        {

            if (id != 0)
            {
                HardWare deleteHardWare = _db.HardWare.Find(id);
                _db.HardWare.Remove(deleteHardWare);
                _db.SaveChanges();


            }


        }

        public void DeleteStock(int id)
        {

            if (id != 0)
            {
                Stock deleteStock = _db.Stock.Find(id);
                _db.Stock.Remove(deleteStock);
                _db.SaveChanges();


            }


        }

        public HardWare UpdateDevice(HardWare hardWare)
        {
            if (hardWare != null)
            {
                HardWare toUpperHardware = new HardWare
                {
                    Id = hardWare.Id,
                    Merk = hardWare.Merk.ToUpper(),
                    Model = hardWare.Model.ToUpper(),
                    Type = hardWare.Type.ToUpper()
                };
                _db.Entry(toUpperHardware).State = EntityState.Modified;
                _db.SaveChanges();
            }
            return hardWare;

        }


        public string GetDate()
        {
            string dateTime = DateTime.Now.ToShortDateString();

            return dateTime;

        }
        public Stock CreateStock(Stock stock)
        {
            if (stock != null)
            {
              
                _db.Stock.Add(stock);
                _db.SaveChanges();
            }

            return stock;

        }

        public Inschrijven CreateinInschrijven(Inschrijven inschrijven)
        {

            Stock st = _db.Stock.Find(inschrijven.StockId);

            inschrijven.Stock = st;


            var aantalgebruiks = st.VoorRaad - inschrijven.Aantal;

            st.VoorRaad = aantalgebruiks;

            _db.Entry(st).State = EntityState.Modified;
            _db.SaveChanges();


            _db.Inschrijven.Add(inschrijven);
            _db.SaveChanges();

            return inschrijven;

        }

        public List<Inschrijven> Doas()
        {
           
            return _db.Inschrijven.Where(itm => itm.Doa != false).ToList();
        }

        public Stock UpdateStock(int id)
        {
           
            var upateStock = _db.Stock.Find(id);
            var hardware = _db.HardWare.Find(upateStock.HardWareId);

            upateStock.HardWare = hardware;

            return upateStock;

        }

        public Stock UpdateStock(Stock updateStock)
        {
            if (updateStock != null)
            {
                HardWare hardWare = _db.HardWare.Find(updateStock.HardWareId);

                updateStock.HardWare = hardWare;

                var prijsIncleBtw = updateStock.IkPrijs * 1.21;
                var prijsMetMarge = prijsIncleBtw / 100 * updateStock.Marge;
                var totaalPrijs = prijsIncleBtw + prijsMetMarge;
                updateStock.Prijs = (float)totaalPrijs;
                updateStock.PersoneelsPrijs = (float)(updateStock.IkPrijs * 1.21 * 1.12);


                _db.Entry(updateStock).State = EntityState.Modified;
                _db.SaveChanges();
            }
            return updateStock;

        }


        public List<string> ListMerken()
        {
            List<string> merken = new List<string>();

            foreach (var item in _db.HardWare)
            {
                if (!merken.Contains(item.Merk))
                    merken.Add(item.Merk);
            }


            return merken.ToList();

        }
        public List<string> ListType()
        {
            List<string> types = new List<string>();

            foreach (var item in _db.HardWare)
            {
                if (!types.Contains(item.Type))
                    types.Add(item.Type);
            }


            return types.ToList();

        }

        public Stock LogUsed(int id, int aantal)
        {
            if (id!=0 && aantal!=0)
            {
                Stock used = _db.Stock.Find(id);

                var voorRaad = used.VoorRaad - aantal;

                used.VoorRaad = voorRaad;

                HardWare noChage = _db.HardWare.Find(used.HardWareId);

                used.HardWare = noChage;

                _db.Entry(used).State = EntityState.Modified;
                _db.SaveChanges();
                return used;
            }

            return null;
        }


        public List<string> GetEnumList()
        {

            statuslist.Add(Status.Aanname.ToString());
            statuslist.Add(Status.Besteld.ToString());
            statuslist.Add(Status.Bezig.ToString());
            statuslist.Add(Status.Hersteld.ToString());
            statuslist.Add(Status.Onhersteld.ToString());


            return statuslist;

        } 


    }
}