﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ClassInventor;

namespace InventorApp.Models
{
    public class CsvConverter
    {

        private string ReadFromCSVFile()
        {
            Tekstbestand bestand = new Tekstbestand();
            bestand.FileName = @"C:\Users\BGNOUTCHE\OneDrive\Documenten\MediaMarkt\HerstelBalieInventorAsp\InventorApp\InventorApp\INVENTOR.csv";
            bestand.Lees();
            return bestand.Text;
        }
        public List<Stock> GetList()
        {

            string[] stocklist = this.ReadFromCSVFile().Split('\n');
            List<Stock> list = new List<Stock>();
            foreach (string s in stocklist)
            {
                list.Add(this.ToObjectStock(s));
            }
            return list;


        }

        public List<HardWare> GetHardwareList()
        {
            string[] stocklist = this.ReadFromCSVFile().Split('\n');
            List<HardWare> list = new List<HardWare>();
            foreach (string s in stocklist)
            {
                list.Add(this.ToObjectHardWare(s));
            }
            return list;
        }

        public Stock ToObjectStock(string line)
        {

            Stock stock = new Stock();
            HardWare hardWare  = new HardWare();
            string[] values = line.Split('|');


                hardWare.Type = values[0];
               hardWare.Merk = values[1];
                hardWare.Model = values[2];


            stock.HardWare = hardWare;
            stock.Naam = values[3];
            stock.Option = values[4];
            stock.Leverancier = values[5];

            stock.MinVoorRaad = int.Parse(values[6]);
            stock.VoorRaad = int.Parse(values[7]);

            stock.Prijs = float.Parse(values[8]);
            stock.IkPrijs = float.Parse(values[9]);
            stock.TransprotKoste = float.Parse(values[10]);
            stock.Marge = float.Parse(values[11]);

            stock.PersoneelsPrijs = float.Parse(values[12]);


            return stock;
        }

        public HardWare ToObjectHardWare(string line)
        {

            HardWare hd = new HardWare();
           
            string[] values = line.Split('|');


            hd.Type = values[0];
            hd.Merk = values[1];
            hd.Model = values[2];

        

            return hd;
        }
    }
}